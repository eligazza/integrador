#^ ---------- LLAVES -------------- ^#
resource "aws_key_pair" "key_pair" {
  key_name   = "id_rsa"
  public_key = file("./id_rsa.pub")
}

#^ ---------- FRONTEND -------------- ^#
resource "aws_instance" "frontend" {
    ami                         = "ami-0fc5d935ebf8bc3bc" # Ubuntu 22.04 LTS / us-east-1
    instance_type               = "t2.micro"
    key_name                    = aws_key_pair.key_pair.key_name
    associate_public_ip_address = true
    subnet_id                   = aws_subnet.example.id
    vpc_security_group_ids      = [aws_security_group.frontend.id] 

    tags = { 
      Name = "1023c05-grupo2-frontend"
    }

    provisioner "remote-exec" {
      inline = [
        "sudo apt-get update -y",
        "sudo apt install docker.io -y",
        "sudo docker run -d -p 8080:80 --name nginx nginx",
      ]

      connection {
        type        = "ssh"
        user        = "ubuntu"
        private_key = file("./id_rsa")
        host        = self.public_ip
      }
    }
}


#^ ---------- BACKEND -------------- ^#
resource "aws_instance" "backend" {
    ami                         = "ami-0fc5d935ebf8bc3bc" # Ubuntu 22.04 LTS / us-east-1
    instance_type               = "t2.micro"
    key_name                    = aws_key_pair.key_pair.key_name
    associate_public_ip_address = true
    subnet_id                   = aws_subnet.example.id
    vpc_security_group_ids      = [aws_security_group.backend.id] 

    tags = {
        Name = "1023c05-grupo2-backend"
    }

    provisioner "remote-exec" {
      inline = [
       "sudo apt-get update -y",
       "sudo apt install docker.io -y",
       "sudo docker run --name mysql-container -e MYSQL_DATABASE=${variables.MYSQL_DATABASE} -e MYSQL_USER=${variables.MYSQL_USER} -e MYSQL_PASSWORD=${variables.MYSQL_PASSWORD} -p 3306:3306 -d mysql:latest",
       "sudo apt install openjdk-17-jre -y" # Debe coincidir con la version de java que use para compilar la app
      ]

      connection {
       type        = "ssh"
       user        = "ubuntu"
       private_key = file("./id_rsa")
       host        = self.public_ip
      }
    }

}
