# Crear servicio

1- Conectarme a la instancia

2- Ir a la carpeta system y crear el servicio con nano
cd /etc/systemd/system/
sudo nano guider.service

3- Así luce el archivo:

[Unit]
Description= Guider Java REST Service @eligazza

[Service]
User=ubuntu
WorkingDirectory=/home/ubuntu
ExecStart=/usr/bin/java -jar /home/ubuntu/Guider-0.0.1-SNAPSHOT.jar
SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target


4- Dar un reload al daemon y arrancar el servicio
sudo systemctl daemon-reload
sudo systemctl enable guider.service
sudo systemctl start guider.service
sudo systemctl status guider.service
