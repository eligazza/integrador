terraform {
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "MYSQL_USER" {
  description = "Name of MYSQL user"
}

variable "MYSQL_DATABASE" {
  description = "MYSQL database name"
}

variable "MYSQL_PASSWORD" {
  description = "Password for MYSQL database"
}