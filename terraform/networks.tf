#? Create a default VPC data source

data "aws_vpc" "default" {
  default = true
}

#? Create a default subnet within the default VPC

resource "aws_subnet" "example" {
  vpc_id     = data.aws_vpc.default.id
  cidr_block = "172.100.0.0/24" # Specify the CIDR block for the subnet
  availability_zone = "us-east-1a" # Specify the desired availability zone

  tags = {
    Name = "MySubnet" # Add a name tag to identify the subnet
  }
}

#? Create 2 custom security groups for the front and backend

resource "aws_security_group" "frontend" {
  name        = "frontend-sg"
  description = "Custom Security Group for Frontend server"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.front_tags
}

resource "aws_security_group" "backend" {
  name        = "backend-sg"
  description = "Custom Security Group for Backend server"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    protocol    = "tcp"
    from_port   = 11000 # java-app port in application_properties
    to_port     = 11000 # java-app port in application_properties
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp" # ssh
    from_port   = 22 
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.front_tags
}


# #? Attach the custom security groups to the subnet

# resource "aws_network_interface_sg_attachment" "frontend_attachment" {
#   security_group_id = aws_security_group.frontend.id
#   network_interface_id = aws_subnet.example.network_interface_id[0]
# }

# resource "aws_network_interface_sg_attachment" "backend_attachment" {
#   security_group_id = aws_security_group.backend.id
#   network_interface_id = aws_subnet.example.network_interface_ids[0]
# }