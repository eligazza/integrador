import React from "react";
import { Select, SelectItem } from "@nextui-org/react";
import { useAuth } from "./Context/AuthContext";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Home from "./Routes/Home/Home";
import CargarTour from "./Routes/Tours/CargarTour";
import PanelAdmin from "./Routes/PanelAdmin/PanelAdmin";
import DetalleProducto from "./Routes/DetalleProducto/DetalleProducto";
import CrearCuenta from "./Routes/CrearCuenta/CrearCuenta";
import Login from "./Routes/Auth/Login";
import Forbidden from "./Routes/Forbidden/Forbidden";
import Perfil from "./Routes/Perfil/perfil";
import AgregarCategoria from "./Routes/Admin/AgregarCategoria";
import EditarTour from "./Routes/Tours/EditarTour";
import EditarUsuario from "./Routes/Admin/EditarUsuario";
import MostrarTours from "./Routes/Tours/MostrarTours";
import Buscar from "./Routes/Buscar/Buscar";
import Reserva from "./Routes/Reservas/reserva";
function App() {
  const { currentUser, logout } = useAuth();
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="admin/panel" element={<PanelAdmin />} />
        <Route
          path="admin/cargar"
          element={
            currentUser?.role == "ADMIN" || currentUser?.role == "GUIDER" ? <CargarTour /> : <Forbidden />
          }
        />
        <Route path="producto/:id" element={<DetalleProducto />} />
        <Route path="registro" element={<CrearCuenta />} />
        <Route path="login/" element={<Login />} />
        <Route path="confirmacion/:id" element={<Reserva />} />
        <Route path="login/:requiered" element={<Login />} />
        <Route path="admin/agregarcategoria" element={currentUser?.role == "ADMIN"  ? <AgregarCategoria /> : <Forbidden />} />
        <Route path="admin/editarusuario" element={currentUser?.role == "ADMIN" ? <EditarUsuario /> : <Forbidden />} />
        <Route path="admin/editar/:id" element={ currentUser?.role == "ADMIN" || currentUser?.role == "GUIDER" ? <EditarTour /> : <Forbidden />} />
        <Route path="admin/editar" element={ currentUser?.role == "ADMIN" || currentUser?.role == "GUIDER"? <MostrarTours /> : <Forbidden />} />
        <Route path="perfil" element={ currentUser ?  <Perfil /> : <Navigate to="/login"/>} />
        <Route path="buscar/:param" element={  <Buscar /> } />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
