import React, { useState, useEffect } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Divider,
  Link,
  Image,
} from "@nextui-org/react";
import { useAuth } from "../../Context/AuthContext";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Pagination,
  getKeyValue,
  User,
  Chip,
  Button
} from "@nextui-org/react";
import { DeleteIcon } from "../../utils/icons.jsx";

const columns = [
  { name: "Fecha", uid: "date" },
  { name: "Hora", uid: "time" },
  { name: "Nombre tour", uid: "title" },
  { name: "Acciones", uid: "actions" },
];

const Perfil = () => {
  const [page, setPage] = React.useState(1);
  const [appointments, setAppointments] = React.useState([]);
  const [favorites, setFavorites] = React.useState([]);
  const { currentUser, API_ROUTE, token } = useAuth();
  const rowsPerPage = 4;

  const pages = Math.ceil(appointments.length / rowsPerPage);

  const items = React.useMemo(() => {
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;
    console.log(appointments);

    return appointments.slice(start, end);
  }, [page, appointments]);
  
  function stringAvatar(user) {
    
    return (
      user.firstName[0] + user.lastName[0]
    );
  }

  const tourAppointments = async () => {
    try {
      const response = await fetch(API_ROUTE + `/appointments/by-user-id?userId=${currentUser.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.ok) {
        const data = await response.json();
        setAppointments(data);
        //setOk(true)
      }else if (response.status === 404) {
        setAppointments([]);
      }
      else {
        //setFail(true)
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } finally {
      //onOpen(true)
    }
  };

  const getFavs = async () => {

    try {
      const response = await fetch(API_ROUTE + `/favs/${currentUser.id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.ok) {
        const data = await response.json();
        console.log(data)
        setFavorites(data);
        //setOk(true)
      }
      else {
        //setFail(true)
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } finally {
      //onOpen(true)
    }
  };

  const handleDelete = async (id) => {
    try {
      const response = await fetch(API_ROUTE + `/appointments/${id}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
        Authorization: `Bearer ${token}`,
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setOk(true);
        userGet()
      } else {
        setFail(true);
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.log(error.response.data);
    } finally {
      tourAppointments();
    }
  };


  useEffect(() => {
    tourAppointments();
    getFavs();
  }, []);

  const renderCell = React.useCallback((user, columnKey) => {
    const cellValue = user[columnKey];

    switch (columnKey) {
      case "date":
        return <p>{user.date}</p>;
      case "time":
        return (
            <p >
              {user.time}
            </p>
        );
      case "title":
        return <p>{user.tour.name}</p>;
        case "actions":
          return (
            <div className="relative flex items-center gap-2">
              <Button isIconOnly onPress={() => handleDelete(user.id)}>
                <DeleteIcon />
              </Button>
            </div>
          );
      default:
        return cellValue;
    }
  }, []);

  return (
    <div className="h-screen flex flex-col justify-between">
      <Header />
      <div className="self-center flex flex-col gap-10">
        <Card className="max-w-[1024px]">
          <CardHeader className="flex justify-center">
            <h1>Mi Perfil</h1>
          </CardHeader>
          <CardBody>
          <User
                    name={currentUser.firstName + " " + currentUser.lastName}
                    description={
     
                        <Chip
                          size="sm"
                          variant="bordered"
                          className="text-[10px] h-5  border-[#06A77D] text-[#06A77D] font-semibold"
                        >
                          {currentUser.role}
                        </Chip>
         
                    }
                    avatarProps={{
                      name: stringAvatar(currentUser),
                      className: `bg-[#06A77D] font-black text-[16px] text-white`,
                    }}
                  />
          </CardBody>
        </Card>

        <div className="flex flex-row gap-10">
          <Card className="max-w-[612px]">
            <CardHeader className="flex justify-center">
              <h1>Mis datos</h1>
            </CardHeader>
            <CardBody className="flex flex-col gap-4">
              <div>
                <h2>Email:</h2>
                <p>{currentUser.email}</p>
              </div>
              <div>
                <h2>Nombre:</h2>
                <p>{currentUser.firstName}</p>
              </div>
              <div>
                <h2>Apellido:</h2>
                <p>{currentUser.lastName}</p>
              </div>
              <div>
                <h2>Role:</h2>
                <p>{currentUser.role}</p>
              </div>
            </CardBody>
          </Card>
          <Card className="max-w-[612px]">
            <CardHeader className="flex justify-center">
              <h1>Mis favoritos</h1>
            </CardHeader>
            <CardBody>
              {favorites ? (
                <h2>
                  Parece que todavia no tenemos favoritos. Empecemos a explorar!
                </h2>
              ) : (
                currentUser.favorites.map((tour) => (
                  <Card
                    key={tour.id}
                    id={tour.id}
                    title={tour.name}
                    price={tour.price}
                    location={tour.location}
                    rating={tour.rating}
                    images={tour.images[0]}
                  />
                ))
              )}
            </CardBody>
          </Card>
        </div>
        <Card className="max-w-[1024px]">
          <CardHeader className="flex justify-center">
            <h1>Mis reservas</h1>
          </CardHeader>
          <CardBody>
            {appointments && (
              <Table aria-label="Example table with custom cells">
                <TableHeader columns={columns}>
                  {(column) => (
                    <TableColumn
                      key={column.uid}
                      align={column.uid === "actions" ? "center" : "start"}
                    >
                      {column.name}
                    </TableColumn>
                  )}
                </TableHeader>
                <TableBody items={appointments}>
                  {(item) => (
                    <TableRow key={item.id}>
                      {(columnKey) => (
                        <TableCell>{renderCell(item, columnKey)}</TableCell>
                      )}
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            )}
          </CardBody>
        </Card>
      </div>

      <Footer />
    </div>
  );
};

export default Perfil;
