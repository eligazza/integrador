import React, { useState, useEffect, useRef } from 'react'
import Header from '../../Components/Header/Header'
import Footer from '../../Components/Footer/Footer';
import { useParams } from 'react-router-dom';
import { useAuth } from '../../Context/AuthContext';
import maplibregl from 'maplibre-gl';
import 'maplibre-gl/dist/maplibre-gl.css';


const Reserva = () => {
  const { id } = useParams();
  const { API_ROUTE } = useAuth()
  const [appointment, setAppointment] = useState();
  const [loading, setLoading] = useState();
  const mapContainer = useRef(null);
  const map = useRef(null);
  const [lng, setLng] = useState(19.753);
  const [lat, setLat] = useState(35.6844);
  const [zoom] = useState(14);
  const [API_KEY] = useState('8NCtlpz68WMujUVdetam');

  const appointmentGet = async () => {
    try {
      const response = await fetch(API_ROUTE + `/appointments/${id}`, {
        method: "GET",
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data)
        setAppointment(data);
        setLat(parseFloat(data.tour.location.latitude))
        setLng(parseFloat(data.tour.location.longitude))
        setLoading(false);
      } else {
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.log(error.response.data);
    }
  };
  useEffect(() => {
    appointmentGet();
  }, []);


  useEffect(() => {
    //if (map.current) return; // stops map from intializing more than once

    map.current = new maplibregl.Map({
      container: mapContainer.current,
      style: `https://api.maptiler.com/maps/streets-v2/style.json?key=${API_KEY}`,
      center: [lng, lat],
      zoom: zoom
    });
    map.current.addControl(new maplibregl.NavigationControl(), 'top-right');
    new maplibregl.Marker({ color: "#FF0000" })
      .setLngLat([lng, lat])
      .addTo(map.current);

  }, [API_KEY, lng, lat, zoom]);
  //#B185A8
  //#06A77D
  //#E06A00
  return (

    <div className='h-screen flex flex-col justify-between'>
      <Header />
      {!loading &&
        <div className='flex flex-col gap-2 justify-center items-center py-8 px-4'>
          <p className='text-[42px] text-[#06A77D] font-semibold'>Felicidades {appointment?.user.firstName}!</p>
          <p className=''>Tu reserva esta confirmada</p>
          <div className='text-center'>
            <p className='text-[36px] text-[#B185A8] font-semibold'>{appointment?.tour.name}</p>
            <img src={appointment?.tour.images[0]} className='h-[250px] object-fit mx-auto'/>
          </div>

          <div className='text-center'>
            <p className='text-[28px] text-[#E06A00] font-semibold'>Ubicacion: </p>
            <div ref={mapContainer} className="w-[350px] h-[350px] relative" />
            <p>{appointment?.tour.location.name}</p>
          </div>

          <div className='text-center w-3/5'>
            <p className='text-[28px] text-[#E06A00] font-semibold'>Descripcion</p>
            <p className=''>{appointment?.tour.description}</p>
          </div>

          <div className='text-center'>
            <p className='text-[28px] text-[#E06A00] font-semibold'>Cuando?</p>
            <p>Fecha: {appointment?.date}</p>
            <p>Hora: {appointment?.time}</p>
          </div>

          <div className='text-center'>
            <p className='text-[28px] text-[#E06A00] font-semibold'>Mis datos.</p>
            <p>Nombre: {appointment?.user.firstName}</p>
            <p>Apellido: {appointment?.user.lastName}</p>
            <p>Mail: {appointment?.user.email}</p>
          </div>
        </div>
      }
      <></>
      <Footer />
    </div>

  )
}

export default Reserva