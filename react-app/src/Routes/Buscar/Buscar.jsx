import React, { useEffect,useRef } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import { useParams } from "react-router-dom";
import { Listbox, ListboxItem } from "@nextui-org/react";
import { useAuth } from "../../Context/AuthContext";
import Card from "../../Components/Card/Card";
const Buscar = () => {
  const [selectedKeys, setSelectedKeys] = React.useState(new Set([]));
  const { param } = useParams();
  const { categories, API_ROUTE, getAllTours, getAllCategories, tours ,getLocations,locations} = useAuth();
  //const [tours, setTours] = React.useState([]);
  const [filterTours, setFilterTours] = React.useState([]);
  const [filterCategory, setFilterCategory] = React.useState([]);
  const [filterLocation, setFilterLocation] = React.useState([]);
  const [currentPage, setCurrentPage] = React.useState(1);

  const isMounted = useRef(true);

  useEffect(() => {
    // Función para obtener el valor después del igual (=)

  }, [param]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await getLocations();
        await getAllTours();
        await getAllCategories();
        const getValueAfterEqual = (paramString) => {
          const paramArray = paramString.split('=');
          return paramArray.length > 1 ? paramArray[paramArray.length - 1] : null;
        };
    
        // Verificar si existe el parámetro en la URL y actualizar los estados
        if (param) {
          if (param.includes('loc=')) {
            // Obtener y actualizar el estado locations
            const locValue = getValueAfterEqual(param);
            const term = locValue.split("=")
            setFilterLocation(term[1]);
          } else if (param.includes('cat=')) {
            // Obtener y actualizar el estado category
            const catValue = getValueAfterEqual(param);
            const term = catValue.split("=")
            console.log(term[1])
            setFilterCategory(term[1]);  //Papi dale mecha a ese blond
          }
        }
        console.log(filterLocation)
        console.log(filterCategory) //Yo cre
//        setLoading(false);
      } catch (error) {
        console.error(error);
 //       setLoading(false);
      }
    };

    if (isMounted.current) {
      fetchData();
      isMounted.current = false;
    }
  }, []); // El segundo argumento [] indica que este efecto solo se ejecutará una vez (equivalente a componentDidMount)

  const itemsPerPage = 10; // Cantidad de tours por página
  const selectedValue = React.useMemo(
    () => Array.from(selectedKeys).join(", "),
    [selectedKeys]
  );

  const filterTour = () => {
    const toursFiltered= tours.filter(tour => {
      const filter1 = tour.location.name === filterLocation;
      const filter2 = tour.category.name === selectedValue.length === 0 || selectedValue.split(", ").includes(tour.category.title);
      return filter1 && filter2;
    });

    return toursFiltered
  };

  useEffect(() => {
    const filteredTours = filterTour();
    console.log(filteredTours)
    setFilterTours(filteredTours);
  }, [selectedValue,]);

  return (
    <div className="h-screen flex flex-col justify-between">
      <Header />
      <div className="flex flex-row">
        <div className="w-[150px]">
          <h3 className="text-center">Categorias</h3>
          <div className="w-fill border-small px-1 py-2 rounded-small border-default-200 dark:border-default-100">
            <Listbox
              aria-label="Multiple selection example"
              variant="flat"
              defaultSelectedKeys={['11']}
              disallowEmptySelection
              selectionMode="multiple"
              selectedKeys={selectedKeys}
              onSelectionChange={setSelectedKeys}
              items={categories}
            >
              {(item) => (
                <ListboxItem key={item.title} textValue={item.title}>
                  <h4>{item.title}</h4>
                </ListboxItem>
              )}
            </Listbox>
          </div>
        </div>
        <div className="flex flex-col">
          <h2 className="bg-[#E06A00] text-center w-3/5 py-2 mx-auto rounded-[16px] text-[12px] text-white my-8">
            Todos nuestros tours
          </h2>
          <div className="flex flex-row flex-wrap gap-8 justify-center">
            {tours &&
              filterTours
                .map((tour) => (
                  <Card
                    key={tour.id}
                    id={tour.id}
                    title={tour.name}
                    price={tour.price}
                    location={tour.location}
                    rating={tour.rating}
                    images={tour.images[0]}
                  />
                ))}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Buscar;
