import React from "react";
import { useEffect, useState, useRef } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import { Input, Textarea } from "@nextui-org/react";
import { Select, SelectItem } from "@nextui-org/react";
import { useForm } from "react-hook-form";
import { Card, CardBody, CardHeader, Image, Button, Autocomplete, AutocompleteItem } from "@nextui-org/react";
import { CheckIcon, TrashIcon, XIcon } from "../../utils/icons";
import {
  useDisclosure,
} from "@nextui-org/react";
import { useAuth } from "../../Context/AuthContext";
import { useAsyncList } from "@react-stately/data";


const CargarTour = () => {
  const { currentUser, categories, locations, getLocations, postOneTour, getAllCategories } = useAuth();
  const [value, setValue] = React.useState("cat");
  const [images, setImages] = useState([]);

  const isMounted = useRef(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await getLocations();
        await getAllCategories();
        //setLoading(false);
      } catch (error) {
        console.error(error);
        //setLoading(false);
      }
    };

    if (isMounted.current) {
      fetchData();
      isMounted.current = false;
    }
  }, []);

  let list = useAsyncList({
    async load({ signal, filterText }) {
      let res = locations.filter(location =>
        location.name.toLowerCase().includes(filterText.toLowerCase())
      );

      if (res.length > 10) {
        res = res.slice(0, 10);
      }
      //let json = await res.json();

      return {
        items: res,
      };
    },
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    defaultValues: {
      rating: 0,
      guideId: currentUser.id,
    },
  });

  const tourSubmit = async (data) => {
    data.locationId = value
    postOneTour(data,images)
  }
  const something = (data) => {
    setValue(data)
    console.log(data)
    //postOneTour(data,images)
  }


  const handleImageDelete = (index) => {
    const newImages = [...images];
    newImages.splice(index, 1);
    setImages(newImages);
  };

  return (
    <div className="h-screen flex flex-col justify-between">
      <Header />
      <h1 className="text-2xl font-semibold text-center">Cargar Tour</h1>
      <form onSubmit={handleSubmit(tourSubmit)}>
        <div className=" hidden lg:flex flex-col p-10 gap-12">


          <div className="flex flex-wrap justify-evenly overflow-hidden  ">
            <div className="w-2/5 flex flex-col gap-8">
              <Input
                isRequired
                type="text"
                label="Nombre del Tour"
                labelPlacement="outside"
                placeholder="Ingrese el nombre de su tour"
                color="warning"
                {...register("name")}
                classNames={{
                  label: "text-[#B185A8] ",
                  input: [
                    "bg-[#F9F3DB]",
                    "text-[#B185A8]",
                    "placeholder:text-[#B185A8]",
                  ],
                  innerWrapper: "bg-[#F9F3DB]",
                  placeholder: "text-[#B185A8]",
                }}
              />
              <div className="flex gap-12">
                <Input
                  isRequired
                  type="number"
                  label="Duracion"
                  labelPlacement="outside"
                  placeholder="00:00"
                  description="Duracion total del tour"
                  color="warning"
                  {...register("durationInHours")}
                  classNames={{
                    label: "text-[#B185A8] ",
                    input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                    placeholder: "text-[#B185A8]",
                    description: "text-[#B185A8]",
                  }}
                  endContent={
                    <div className="pointer-events-none flex items-center">
                      <span className="text-default-400 text-small text-[#B185A8]">
                        hrs
                      </span>
                    </div>
                  }
                />
                <Input
                  isRequired
                  type="number"
                  label="Precio"
                  labelPlacement="outside"
                  placeholder="0.00"
                  color="warning"
                  {...register("price")}
                  classNames={{
                    label: "text-[#B185A8] ",
                    input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                    placeholder: "text-[#B185A8]",
                  }}
                  startContent={
                    <div className="pointer-events-none flex items-center">
                      <span className="text-default-400 text-small text-[#B185A8]">
                        $
                      </span>
                    </div>
                  }
                />
              </div>
              <Autocomplete
                className="max-w-xs"
                isRequired
                onSelectionChange={setValue}
                inputValue={list.filterText}
                isLoading={list.isLoading}
                items={list.items}
                label="Seleccione una ubicacion"
                placeholder="Escriba la ubicacion..."
                color="warning"
                onInputChange={list.setFilterText}
                {...register("locationId")}
              >
                {(item) => (
                  <AutocompleteItem key={item.id} value={item.id}>
                    {item.name}
                  </AutocompleteItem>
                )}
              </Autocomplete>
              <Select
                isRequired
                label="Categoria"
                placeholder="Seleccione una categoria"
                labelPlacement="outside"
                className="max-w-xs"
                color="warning"
                {...register("categoryId")}
                classNames={{
                  label: "text-[#B185A8] ",
                  input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                  placeholder: "text-[#B185A8]",
                  renderValue: [
                    "text-[#B185A8]",
                    "bg-[#F9F3DB]",
                    "hover:bg-[#F9F3DB]",
                  ],
                  popover: [
                    "text-[#B185A8]",
                    "bg-[#F9F3DB]",
                    "hover:bg-[#F9F3DB]",
                  ],
                  value: ["text-[#B185A8]"],
                }}
              >
                {categories.map((category) => (
                  <SelectItem
                    key={category.id}
                    value={category.value}
                  >
                    {category.title}
                  </SelectItem>
                ))}
              </Select>
            </div>
            <div className="w-2/5 flex flex-col gap-6">
              <Textarea
                isRequired
                color="warning"
                minRows={4}
                label="Descripcion"
                labelPlacement="outside"
                placeholder="Ingrese una descripcion detallada de su tour"
                {...register("description")}
                classNames={{
                  label: "text-[#B185A8] ",
                  input: [
                    "bg-[#F9F3DB]",
                    "text-[#B185A8]",
                    "placeholder:text-[#B185A8]",
                  ],
                  innerWrapper: "bg-[#F9F3DB]",
                  placeholder: "text-[#B185A8]",
                }}
              />
              <h2 className="text-[#B185A8]">Imagenes</h2>

              <div className="flex gap-10 flex-wrap">
                {Array.from(images).map((image, index) => (
                  <Card shadow="sm" key={index} className="w-[100px]">
                    <CardHeader className="absolute z-10 top-0 left-11 flex-col !items-start">
                      <Button

                        isIconOnly
                        className="bg-[#dc3545] text-white"
                        size="sm"
                        onClick={() => handleImageDelete(index)}
                      >
                        <TrashIcon />
                      </Button>
                    </CardHeader>
                    <CardBody className="overflow-visible p-0">
                      <Image
                        shadow="sm"
                        radius="lg"
                        className="z-0 w-full h-full object-cover"
                        src={URL.createObjectURL(image)}
                      />
                    </CardBody>
                  </Card>
                ))}
              </div>

              <input
                required
                type="file"
                multiple
                onChange={(e) => setImages(e.target.files)}
                accept="image/*"
                className="block w-full text-sm text-slate-500
      file:mr-4 file:py-2 file:px-4
      file:rounded-md file:border-0
      file:text-sm 
      file:bg-[#E06A00] file:text-white
      hover:file:bg-[#E06a00]
    "
              />
            </div>
          </div>
          <Button
            type="submit"
            size="sm"
            className="w-[50px] self-center bg-[#E06A00] text-white"
          >
            Subir Tour
          </Button>
        </div>
      </form>
      <Footer />

    </div>
  );
};

export default CargarTour;
