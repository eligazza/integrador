import React, { useEffect, useState, useRef } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Link,
  Button,
  useDisclosure,
} from "@nextui-org/react";
import { DeleteIcon } from "../../utils/icons";
import { EyeIcon } from "../../utils/icons";
import { EditIcon } from "../../utils/icons";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import { useAuth } from "../../Context/AuthContext";

const columns = [
  { name: "ID", uid: "id", sortable: true },
  { name: "Nombre del tour", uid: "name", sortable: true },
  { name: "Categoria", uid: "category", sortable: true },
  { name: "Ubicacion", uid: "location", sortable: true },
  { name: "Acciones", uid: "actions" },
];

const MostrarTours = () => {
  const { deleteOneTour,getMyTours,tours } = useAuth();
  const [loading, setLoading] = useState(true);
  const isMounted = useRef(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await getMyTours();
        setLoading(false);
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    };

    if (isMounted.current) {
      fetchData();
      isMounted.current = false;
    }
  }, []); // El segundo argumento [] indica que este efecto solo se ejecutará una vez (equivalente a componentDidMount)

  const renderCell = React.useCallback((tours, columnKey) => {
    const cellValue = tours[columnKey];

    switch (columnKey) {
      case "name":
        return <p>{tours.name}</p>;
      case "category":
        return <p>{tours.category.title}</p>;
      case "location":
        return <p>{tours.location.name}</p>;
      case "actions":
        return (
          <div className="relative flex items-center gap-2">
            <div className="relative flex items-center gap-2">
              <Button isIconOnly onPress={() => deleteOneTour(tours.id)}>
                <DeleteIcon />
              </Button>
              <Button isIconOnly as={Link} href={`/admin/editar/${tours.id}`}>
                <EditIcon />
              </Button>
            </div>
          </div>
        );
      default:
        return cellValue;
    }
  }, []);

  return (
    <div className="h-screen flex flex-col justify-between">
      <Header />
      <div className="flex flex-col gap-8">
        <h1 className="text-center">Mis tours</h1>
        {!loading && (
          <Table aria-label="Example table with custom cells" className="px-20">
            <TableHeader columns={columns}>
              {(column) => (
                <TableColumn
                  key={column.uid}
                  align={column.uid === "actions" ? "center" : "start"}
                >
                  {column.name}
                </TableColumn>
              )}
            </TableHeader>
            <TableBody
              items={tours}
            >
              {(item) => (
                <TableRow key={item.id}>
                  {(columnKey) => (
                    <TableCell>{renderCell(item, columnKey)}</TableCell>
                  )}
                </TableRow>
              )}
            </TableBody>
          </Table>
        )}
      </div>
      <Footer />
    </div>
  );
};

export default MostrarTours;
