import React, { useEffect, useState } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import SigIn from "../../Components/auth/signIn";
import { Input, Textarea } from "@nextui-org/react";
import { Select, SelectItem } from "@nextui-org/react";
import { countries } from "../../dataTest/data";
import { useForm } from "react-hook-form";
import { uploadCloudinary } from "../../uploadCloudinary";
import { useParams } from "react-router-dom";
import axios from "axios";
import { TrashIcon } from "../../utils/icons";
import { Card, CardBody, CardHeader, Image, Button, Autocomplete,AutocompleteItem  } from "@nextui-org/react";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
} from "@nextui-org/react";
import { useAuth } from "../../Context/AuthContext";
import SuccessAlert from "../../Components/Alerts/Success";
import FailAlert from "../../Components/Alerts/Fail";
import {useAsyncList} from "@react-stately/data";

const EditarTour = () => {
  const [isLoading, setIsLoading] = useState(true);
  const { id } = useParams();
  const [tour, setTour] = useState(null);
  const [ok, setOk] = useState(false);
  const [fail, setFail] = useState(false);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [images, setImages] = useState([]);
  const [url, setUrl] = useState([]);
  const { token, API_ROUTE, categories ,locations} = useAuth();

  let list = useAsyncList({
    async load({signal, filterText}) {
      let res = locations.filter(location =>
        location.name.toLowerCase().includes(filterText.toLowerCase())
      );

      if (res.length > 10) {
        res = res.slice(0, 10);
      }
      //let json = await res.json();

      return {
        items: res,
      };
    },
  });

  const handleImageDelete = (index) => {
    const newImages = [...images];
    newImages.splice(index, 1);
    setImages(newImages);
  };
  const handleOldImageDelete = (image) => {
    const newImages = url.filter((elemento) => elemento !== image);
    setUrl(newImages);
  };

  const handleOldImageRestore = (image) => {
    const newImages = [...url, image];
    setUrl(newImages);
  };

  const tourGet = async () => {
    try {
      const data = await axios.get(API_ROUTE + `/tours/${id}`);
      setTour(data.data);
      console.log(data.data)
      setUrl(data.data.images);
    } catch (error) {
      console.log(error);
    } finally {
      console.log(url);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    tourGet();
  }, []);

  const tourSubmit = async (form) => {
    form.id = parseInt(id);
    form.guideId = tour.guide.id;
    form.rating = tour.rating;
    form.categoryId = parseInt(form.categoryId);
    form.price = parseInt(form.price);
    form.durationInHours = parseInt(form.durationInHours);
    const objetoEncontrado = locations.find(objeto => objeto.name === form.locationId);
    form.locationId = parseInt(objetoEncontrado.id)
    try {
      let arr = [];
      for (let i = 0; i < images.length; i++) {
        const data = await uploadCloudinary(images[i]);
        arr.push(data);
      }
      const allUrls = [...url, ...arr];
      setUrl(allUrls);
      form.images = allUrls;

      console.log(form);
      const response = await fetch(API_ROUTE + "/tours", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(form),
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data);

        setOk(true);
        //window.location.href = "/";
      } else {
        setFail(true);
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.log(error.response.data);
    } finally {
      onOpen(true);
    }
  };
  function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  //console.log(images);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    defaultValues: {},
  });

  return (
    <div className="h-screen flex flex-col justify-between">
      <Header />
      {!isLoading && (
        <form onSubmit={handleSubmit(tourSubmit)}>
          <div className=" hidden lg:flex flex-col p-10 gap-12">
            <h1 className="text-2xl font-semibold text-center">
              Modificar Tour
            </h1>

            <div className="flex flex-wrap justify-evenly overflow-hidden  ">
              <div className="w-2/5 flex flex-col gap-8">
                <Input
                  isRequired
                  defaultValue={tour?.name}
                  type="text"
                  label="Nombre del Tour"
                  labelPlacement="outside"
                  placeholder="Ingrese el nombre de su tour"
                  color="warning"
                  {...register("name")}
                  classNames={{
                    label: "text-[#B185A8] ",
                    input: [
                      "bg-[#F9F3DB]",
                      "text-[#B185A8]",
                      "placeholder:text-[#B185A8]",
                    ],
                    innerWrapper: "bg-[#F9F3DB]",
                    placeholder: "text-[#B185A8]",
                  }}
                />
                <div className="flex gap-12">
                  <Input
                    isRequired
                    defaultValue={tour?.durationInHours}
                    type="number"
                    label="Duracion"
                    labelPlacement="outside"
                    placeholder="00:00"
                    description="Duracion total del tour"
                    color="warning"
                    {...register("durationInHours")}
                    classNames={{
                      label: "text-[#B185A8] ",
                      input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                      placeholder: "text-[#B185A8]",
                      description: "text-[#B185A8]",
                    }}
                  />
                  <Input
                    isRequired
                    type="number"
                    label="Precio"
                    labelPlacement="outside"
                    placeholder="0.00"
                    color="warning"
                    defaultValue={tour?.price}
                    {...register("price")}
                    classNames={{
                      label: "text-[#B185A8] ",
                      input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                      placeholder: "text-[#B185A8]",
                    }}
                    startContent={
                      <div className="pointer-events-none flex items-center">
                        <span className="text-default-400 text-small text-[#B185A8]">
                          $
                        </span>
                      </div>
                    }
                  />
                </div>
                <Autocomplete
      className="max-w-xs"
      isRequired
      labelPlacement="outside"
      inputValue={list.filterText}
      isLoading={list.isLoading}
      items={list.items}
      label="Seleccione una ubicacion"
      placeholder="Escriba la ubicacion..."
      color="warning"
      onInputChange={list.setFilterText}
      defaultSelectedKey = {tour.location.name}
      {...register("locationId")}
    >
      {(item) => (
        <AutocompleteItem key={item.name} value={item.id} className="capitalize">
          {item.name}
        </AutocompleteItem>
      )}
    </Autocomplete>
                
                <h2 className="text-[#B185A8]">Imagenes cargadas</h2>
                <div className="flex gap-10 flex-wrap">
                  {tour.images.map((image, index) => (
                    <Card shadow="sm" key={index} className="w-[100px]">
                      <CardHeader className="absolute z-10 top-0 left-11 flex-col !items-start">
                        {url.includes(image) ? (
                          <Button
                            isIconOnly
                            className="bg-[#dc3545] text-white"
                            size="sm"
                            onClick={() => handleOldImageDelete(image)}
                          >
                            <TrashIcon />
                          </Button>
                        ) : (
                          <Button
                            isIconOnly
                            color="success"
                            className=""
                            size="sm"
                            onClick={() => handleOldImageRestore(image)}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="currentColor"
                              className="w-6 h-6 text-white"
                            >
                              <path

                                d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                              />
                            </svg>
                          </Button>
                        )}
                      </CardHeader>
                      <CardBody className="overflow-visible p-0">
                        <Image
                          shadow="sm"
                          radius="lg"
                          className={`z-0 w-full h-full object-cover ${
                            !url.includes(image) && "brightness-50"
                          }  `}
                          src={image}
                        />
                      </CardBody>
                    </Card>
                  ))}
                </div>

              </div>
              <div className="w-2/5 flex flex-col gap-6">
              <Select
                  isRequired
                  label="Categoria"
                  placeholder="Seleccione una categoria"
                  labelPlacement="outside"
                  className="max-w-xs"
                  color="warning"
                  defaultSelectedKeys={[tour?.category.id.toString()]}
                  {...register("categoryId")}
                  classNames={{
                    label: "text-[#B185A8] ",
                    input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                    placeholder: "text-[#B185A8]",
                    popover: ["text-[#B185A8]", "bg-[#F9F3DB]"],
                    value: ["text-[#B185A8]", "hover:bg-[#F9F3DB]"],
                  }}
                >
                  {categories.map((category) => (
                    <SelectItem
                      key={category.id.toString()}
                      value={category.id}
                    >
                      {category.title}
                    </SelectItem>
                  ))}
                </Select>
                <Textarea
                  color="warning"
                  minRows={4}
                  label="Descripcion"
                  labelPlacement="outside"
                  placeholder="Ingrese una descripcion detallada de su tour"
                  {...register("description")}
                  defaultValue={tour?.description}
                  classNames={{
                    label: "text-[#B185A8] ",
                    input: [
                      "bg-[#F9F3DB]",
                      "text-[#B185A8]",
                      "placeholder:text-[#B185A8]",
                    ],
                    innerWrapper: "bg-[#F9F3DB]",
                    placeholder: "text-[#B185A8]",
                  }}
                />
                

                <h2 className="text-[#B185A8]">Imagenes nuevas</h2>
                <div className="flex gap-10 flex-wrap">
                  {Array.from(images).map((image, index) => (
                    <Card shadow="sm" key={index} className="w-[100px]">
                      <CardHeader className="absolute z-10 top-0 left-11 flex-col !items-start">
                        <Button
                          isIconOnly
                          className="bg-[#dc3545] text-white"
                          size="sm"
                          onClick={() => handleImageDelete(index)}
                        >
                          <TrashIcon />
                        </Button>
                      </CardHeader>
                      <CardBody className="overflow-visible p-0">
                        <Image
                          shadow="sm"
                          radius="lg"
                          className="z-0 w-full h-full object-cover"
                          src={URL.createObjectURL(image)}
                        />
                      </CardBody>
                    </Card>
                  ))}
                </div>

                <input
                  type="file"
                  multiple
                  onChange={(e) => setImages(e.target.files)}
                  accept="image/*"
                  className="block w-full text-sm text-slate-500
      file:mr-4 file:py-2 file:px-4
      file:rounded-md file:border-0
      file:text-sm 
      file:bg-[#E06A00] file:text-white
      hover:file:bg-[#E06a00]
    "
                />
              </div>
            </div>
            <Button
              type="submit"
              size="sm"
              className="w-[100px] self-center bg-[#E06A00] text-white"
            >
              Modificar Tour
            </Button>
          </div>
        </form>
      )}
      <>
        <SuccessAlert
          isOpen={isOpen && ok}
          onOpenChange={onOpenChange}
          onOpen={onOpen}
        />
        <FailAlert
          isOpen={isOpen && fail}
          onOpenChange={onOpenChange}
          onOpen={onOpen}
        />
      </>
      <Footer />
    </div>
  );
};

export default EditarTour;
