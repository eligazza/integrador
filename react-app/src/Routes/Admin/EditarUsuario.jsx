import React ,{useState}from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Input,
  Button,
  DropdownTrigger,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Select,
  SelectItem,
  User,
  Pagination,
  useDisclosure,
} from "@nextui-org/react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import {
  SearchIcon,
  ChevronDownIcon,
  EyeIcon,
  DeleteIcon,
  EditIcon,
} from "../../utils/icons";
import { useAuth } from "../../Context/AuthContext";
import axios from "axios";
import SuccessAlert from "../../Components/Alerts/Success";
import FailAlert from "../../Components/Alerts/Fail";

export const columns = [
  {name: "ID", uid: "id", sortable: true},
  {name: "Nombre", uid: "name", sortable: true},
  {name: "Role", uid: "role"},
  {name: "Email", uid: "email"},
  {name: "Acciones", uid: "actions"},
];



export const roleOptions = [
  {name: "ADMIN", uid: "ADMIN"},
  {name: "GUIDER", uid: "GUIDER"},
  {name: "USER", uid: "USER"},
];


function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function stringAvatar(user) {
    
  return (
    user.firstName[0] + user.lastName[0]
  );
}

export default function EditarUsuario() {
  const [ok, setOk] = useState(false);
  const [fail, setFail] = useState(false);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const {token, currentUser, API_ROUTE} = useAuth()
  const [value, setValue] = React.useState();
  const [filterValue, setFilterValue] = React.useState("");
  const [oldUser, setoldUser] = React.useState();
  const [selectedKeys, setSelectedKeys] = React.useState(new Set([]));
  const [roleFilter, setRoleFilter] = React.useState("all");
  const rowsPerPage = 5;
  const [sortDescriptor, setSortDescriptor] = React.useState({
    column: "id",
    direction: "ascending",
  });
  const [page, setPage] = React.useState(1);

  const [users, setUsers] = React.useState([]);

  const userGet = async () => {

    try {
      const response = await fetch(API_ROUTE + "/users", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`,
        },
      });
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
        
        //setOk(true)
      } else {
        //setFail(true)
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } finally {
      //onOpen(true)
    }
  };
  React.useEffect(() => {
    userGet();
  }, []);

  const hasSearchFilter = Boolean(filterValue);

  const filteredItems = React.useMemo(() => {
    let filteredUsers = [...users];

    if (hasSearchFilter) {
      filteredUsers = filteredUsers.filter((user) =>
        user.name.toLowerCase().includes(filterValue.toLowerCase())
      );
    }
    if (
      roleFilter !== "all" &&
      Array.from(roleFilter).length !== roleOptions.length
    ) {
      filteredUsers = filteredUsers.filter((user) =>
        Array.from(roleFilter).includes(user.role)
      );
    }
    return filteredUsers;
  }, [users, filterValue, roleFilter]);

  const pages = Math.ceil(filteredItems.length / rowsPerPage);

  const items = React.useMemo(() => {
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;

    return filteredItems.slice(start, end);
  }, [page, filteredItems, rowsPerPage]);

  const sortedItems = React.useMemo(() => {
    return [...items].sort((a, b) => {
      const first = a[sortDescriptor.column];
      const second = b[sortDescriptor.column];
      const cmp = first < second ? -1 : first > second ? 1 : 0;

      return sortDescriptor.direction === "descending" ? -cmp : cmp;
    });
  }, [sortDescriptor, items]);

  const renderCell = React.useCallback((user, columnKey) => {
    const cellValue = user[columnKey];

    switch (columnKey) {
      case "name":
        return (
          <User
          avatarProps={{
            name: stringAvatar(user),
            className: `bg-[#06A77D] font-black text-[16px] text-white`,
          }}
            description={user.email}
            name={user.firstName + " " + user.lastName}
          >
            {user.email}
          </User>
        );

      case "role":
        return (
          <Select
            aria-label
            variant="bordered"
            placeholder="Select an animal"
            defaultSelectedKeys={[user.role]}
            selectedKeys={value}
            className="max-w-xs"
            value={user.role}
            onChange={(selectedRole) => handleRoleChange(user.id, selectedRole)}
          >
            {roleOptions.map((role) => (
              <SelectItem key={role.name} value={role.uid}>
                {role.name}
              </SelectItem>
            ))}
          </Select>
        );
      case "actions":
        return (
          <div className="relative flex items-center gap-2">
            <Button isIconOnly onPress={() => handleDelete(user.id)}>
              <DeleteIcon />
            </Button>
          </div>
        );
      default:
        return cellValue;
    }
  }, []);
  const handleDelete = async (userId) => {
    try {
      const response = await fetch(API_ROUTE + `/users/${userId}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setOk(true);
        userGet()
      } else {
        setFail(true);
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.log(error.response.data);
    } finally {
      onOpen(true);
    }
  };

  const handleRoleChange = async (id, selectRole) => {
    const headers = {
       Authorization: `Bearer ${token}`, // Reemplaza 'tuToken' con tu token de autenticación
      'Content-Type': 'application/json', // Puedes ajustar el tipo de contenido según tus necesidades
    };
    const dataaa = await axios.get( API_ROUTE + "/users", { headers });

    try {
      let oldUser = dataaa.data.find((user) => user.id === parseInt(id));
      console.log(oldUser)
      let newUser = {}
      newUser.firstName = oldUser.firstName
      newUser.lastName = oldUser.lastName
      newUser.email = oldUser.email
      newUser.id = oldUser.id
      newUser.role = selectRole.target.value
      console.log(newUser)
      const response = await fetch(API_ROUTE + `/users`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        
        body: JSON.stringify(newUser),
      });
      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setOk(true);
        userGet()
      } else {
        setFail(true);
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.log(error);
    } finally {
      onOpen(true);
    }
  };

  const onSearchChange = React.useCallback((value) => {
    if (value) {
      setFilterValue(value);
      setPage(1);
    } else {
      setFilterValue("");
    }
  }, []);

  const onClear = React.useCallback(() => {
    setFilterValue("");
    setPage(1);
  }, []);

  const topContent = React.useMemo(() => {
    return (
      <div className="flex flex-col gap-4">
        <div className="flex gap-3 justify-end items-end pr-3">
          <Input
            isClearable
            className="w-full sm:max-w-[44%]"
            placeholder="Search by name..."
            startContent={<SearchIcon />}
            value={filterValue}
            onClear={() => onClear()}
            onValueChange={onSearchChange}
          />
          <div className="flex gap-3">
            <Dropdown>
              <DropdownTrigger className="hidden sm:flex">
                <Button
                  endContent={<ChevronDownIcon className="text-small" />}
                  variant="flat"
                >
                  Role
                </Button>
              </DropdownTrigger>
              <DropdownMenu
                disallowEmptySelection
                aria-label="Table Columns"
                closeOnSelect={false}
                selectedKeys={roleFilter}
                selectionMode="multiple"
                onSelectionChange={setRoleFilter}
              >
                {roleOptions.map((role) => (
                  <DropdownItem key={role.uid} className="capitalize">
                    {capitalize(role.name)}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </Dropdown>
          </div>
        </div>
        <div className="flex justify-between items-center">
          <span className="text-default-400 text-small">
            Total {users.length} users
          </span>
        </div>
      </div>
    );
  }, [filterValue, roleFilter, users.length, onSearchChange, hasSearchFilter]);

  const bottomContent = React.useMemo(() => {
    return (
      <div className="py-2 px-2 flex justify-between items-center self-center">
        <Pagination
          className="py-10 self-center"
          classNames={{
            cursor: "bg-[#B185A8]   font-bold",
          }}
          showControls
          page={page}
          total={pages}
          onChange={setPage}
        />
      </div>
    );
  }, [selectedKeys, items.length, page, pages, hasSearchFilter]);

  return (
    <div className="h-screen flex flex-col justify-between">
      <Header />
      {!users?(<p>Cargando</p>):(
      <Table
        aria-label="Example table with custom cells, pagination and sorting"
        isHeaderSticky
        bottomContent={bottomContent}
        bottomContentPlacement="outside"
        className="px-8"
        classNames={{
          wrapper: "max-h-[382px]",
        }}
        selectedKeys={selectedKeys}
        sortDescriptor={sortDescriptor}
        topContent={topContent}
        topContentPlacement="outside"
        onSelectionChange={setSelectedKeys}
        onSortChange={setSortDescriptor}
      >
        <TableHeader columns={columns}>
          {(column) => (
            <TableColumn
              key={column.uid}
              align={column.uid === "actions" ? "center" : "start"}
              allowsSorting={column.sortable}
            >
              {column.name}
            </TableColumn>
          )}
        </TableHeader>
        <TableBody emptyContent={"No users found"} items={sortedItems}>
          {(item) => (
            <TableRow key={item.id}>
              {(columnKey) => (
                <TableCell>{renderCell(item, columnKey)}</TableCell>
              )}
            </TableRow>
          )}
        </TableBody>
      </Table>
      )}

<>
        <SuccessAlert
          isOpen={isOpen && ok}
          onOpenChange={onOpenChange}
          onOpen={onOpen}
        />
        <FailAlert
          isOpen={isOpen && fail}
          onOpenChange={onOpenChange}
          onOpen={onOpen}
        />
      </>
      <Footer />
    </div>
  );
}
