import React from "react";
import { useRef, ChangeEvent, useState } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import { Input, Textarea } from "@nextui-org/react";
import { useForm } from "react-hook-form";
import { uploadCloudinary } from "../../uploadCloudinary";
import axios from "axios";
import { Card, CardBody, CardHeader, Image, Button } from "@nextui-org/react";
import { CheckIcon, TrashIcon, XIcon } from "../../utils/icons";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Pagination,
  getKeyValue,
} from "@nextui-org/react";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
} from "@nextui-org/react";
import { useAuth } from "../../Context/AuthContext";

export const columns = [
  { name: "ID", uid: "id", sortable: true },
  { name: "Titulo", uid: "title", sortable: true },
  { name: "Descripcion", uid: "description", sortable: true },
  { name: "Imagen", uid: "image" },
];

const AgregarCategoria = () => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const { getAllCategories,postOneCategory, categories } = useAuth();
  const [images, setImages] = useState([]);
  console.log(categories)
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    defaultValues: {},
  });

  const [page, setPage] = React.useState(1);
  const rowsPerPage = 3;

  const pages = Math.ceil(categories.length / rowsPerPage);

  const items = React.useMemo(() => {
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;

    return categories.slice(start, end);
  }, [page, categories]);

  React.useEffect(() => {
    getAllCategories();
  }, []);

  const categorySubmit = async (form) => {
    form.image = await uploadCloudinary(images[0]);
    postOneCategory(form)
  };

  const renderCell = React.useCallback((categories, columnKey) => {
    const cellValue = categories[columnKey];

    switch (columnKey) {
      case "title":
        return (
          <p className="text-bold text-sm capitalize text-default-400">
            {categories.title}
          </p>
        );
      case "description":
        return (
          <p className="text-bold text-sm capitalize text-default-400">
            {categories.description}
          </p>
        );
      case "image":
        return <img className="w-24 h-24" src={categories.image} />;
      default:
        return cellValue;
    }
  }, []);

  return (
    <div className="h-screen flex flex-col justify-between">
      <Header />
      <h1 className="text-2xl font-semibold text-center">Administracion de categorias</h1>
      <div>
        <Button onPress={onOpen} size="sm">
          Agregar categoria
        </Button>
      </div>

      {categories && (
        <Table
          aria-label="Example table with client side pagination"
          bottomContent={
            <div className="flex w-full justify-center">
              <Pagination
                isCompact
                showControls
                showShadow
                color="secondary"
                page={page}
                total={pages}
                onChange={(page) => setPage(page)}
              />
            </div>
          }
          classNames={{
            wrapper: "min-h-[222px]",
          }}
        >
          <TableHeader columns={columns}>
            {(column) => (
              <TableColumn
                key={column.uid}
                align={column.uid === "actions" ? "center" : "start"}
              >
                {column.name}
              </TableColumn>
            )}
          </TableHeader>
          <TableBody items={categories}>
            {(item) => (
              <TableRow key={item.id}>
                {(columnKey) => (
                  <TableCell>{renderCell(item, columnKey)}</TableCell>
                )}
              </TableRow>
            )}
          </TableBody>
        </Table>
      )}

      <Footer />
      <Modal isOpen={isOpen} onOpenChange={onOpenChange} placement="top-center">
        <ModalContent>
          {(onClose) => (
            <form onSubmit={handleSubmit(categorySubmit)}>
              <ModalHeader className="flex flex-col gap-1 text-[#06A77D] ">Agrege una nueva categoria</ModalHeader>
              <ModalBody>

                    <div className="w-fill flex flex-col gap-8 align-center">
                      <Input
                        type="text"
                        label="Nombre de la categoria"
                        labelPlacement="outside"
                        placeholder="Ingrese el nombre para su categoria"
                        color="warning"
                        {...register("title")}
                        classNames={{
                          label: "text-[#B185A8] ",
                          input: [
                            "bg-[#F9F3DB]",
                            "text-[#B185A8]",
                            "placeholder:text-[#B185A8]",
                          ],
                          innerWrapper: "bg-[#F9F3DB]",
                          placeholder: "text-[#B185A8]",
                        }}
                      />
                      <Textarea
                        color="warning"
                        minRows={4}
                        label="Descripcion"
                        labelPlacement="outside"
                        placeholder="Ingrese una breve descripcion de su categoria"
                        {...register("description")}
                        classNames={{
                          label: "text-[#B185A8] ",
                          input: [
                            "bg-[#F9F3DB]",
                            "text-[#B185A8]",
                            "placeholder:text-[#B185A8]",
                          ],
                          innerWrapper: "bg-[#F9F3DB]",
                          placeholder: "text-[#B185A8]",
                        }}
                      />

                      <h2 className="text-[#B185A8]">Imagenes</h2>

                      <div className="flex gap-10 flex-wrap">
                        {Array.from(images).map((image, index) => (
                          <Card shadow="sm" key={index} className="w-[100px]">
                            <CardHeader className="absolute z-10 top-0 left-11 flex-col !items-start">
                              <Button
                                isIconOnly
                                className="bg-[#dc3545] text-white"
                                size="sm"
                                onClick={() => handleImageDelete(index)}
                              >
                                <TrashIcon />
                              </Button>
                            </CardHeader>
                            <CardBody className="overflow-visible p-0">
                              <Image
                                shadow="sm"
                                radius="lg"
                                className="z-0 w-full h-full object-cover"
                                src={URL.createObjectURL(image)}
                              />
                            </CardBody>
                          </Card>
                        ))}
                      </div>

                      <input
                        type="file"
                        onChange={(e) => setImages(e.target.files)}
                        accept="image/*"
                        className="block w-full text-sm text-slate-500
      file:mr-4 file:py-2 file:px-4
      file:rounded-md file:border-0
      file:text-sm 
      file:bg-[#E06A00] file:text-white
      hover:file:bg-[#E06a00]
    "
                      />
                    </div>
       
              </ModalBody>
              <ModalFooter>
                <Button
                  className="bg-[#06A77D] text-white font-semibold"
                  variant="flat"
                  onPress={onClose}
                >
                  Cerrar
                </Button>
                <Button
                  type="submit"
                  className="w-[50px] self-center bg-[#E06A00] text-white"
                >
                  Subir Tour
                </Button>
              </ModalFooter>
            </form>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default AgregarCategoria;
