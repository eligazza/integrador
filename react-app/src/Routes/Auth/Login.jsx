import React from 'react'
import Header from '../../Components/Header/Header'
import Footer from '../../Components/Footer/Footer';
import LogIn from '../../Components/auth/logIn';
import { useParams } from 'react-router-dom';
import {Chip} from "@nextui-org/react";
const Login = () => {
  const {requiered} = useParams();
  console.log(requiered)
  return (
    
    <div className='h-screen flex flex-col justify-between '>
        <Header/>
        {requiered && <Chip color="danger" variant="bordered" className='self-center mt-4'>Necesita iniciar sesion para realizar esta accion.</Chip> }
        <LogIn/>
        <Footer/>
    </div>
  )
}

export default Login