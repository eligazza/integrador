import React from "react";
export function usePokemonList(locations) {
  const [items, setItems] = React.useState([]);
  const [hasMore, setHasMore] = React.useState(true);
  const [isLoading, setIsLoading] = React.useState(false);
  const [offset, setOffset] = React.useState(0);
  const limit = 10; // Number of items per page, adjust as necessary
  const loadPokemon = async (currentOffset) => {
    const controller = new AbortController();
    const {signal} = controller;
    
    try {
      setIsLoading(true);


      let json = locations.slice(0,currentOffset+limit)

      //setHasMore(json.next !== null);
      // Append new results to existing ones
      setItems(json);
    } catch (error) {
      if (error.name === "AbortError") {
        console.log("Fetch aborted");
      } else {
        console.error("There was an error with the fetch operation:", error);
      }
    } finally {
      setIsLoading(false);
    }
  };

  React.useEffect(() => {
    loadPokemon(offset);
  }, []);

  const onLoadMore = () => {
    const newOffset = offset + limit;
    //setItems(locations.slice(0,newOffset+limit))
    setOffset(newOffset);
    loadPokemon(newOffset);
    //setHasMore(true)
  };

  return {
    items,
    hasMore,
    isLoading,
    onLoadMore,
  };
};