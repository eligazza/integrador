import React, { useState, useEffect, useRef } from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import Card from "../../Components/Card/Card";
import CardFavorite from "../../Components/Card/CardFavorite";
import CardCategory from "../../Components/Card/CardCategory";
import { Input } from "@nextui-org/react";
import { Pagination, Button } from "@nextui-org/react";
import { Autocomplete, AutocompleteItem } from "@nextui-org/react";
import {
  CalendarIcon,
  LocationIcon,
  SearchIcon,
  CloseIcon,
} from "../../utils/icons.jsx";
import { useAuth } from "../../Context/AuthContext.jsx";
import LoadingPage from "../../Components/Loading/LoadingPage.jsx";
import { useAsyncList } from "@react-stately/data";

const Home = () => {
  const [open, setOpen] = useState(true);
  const [currentCategoryIndex, setCurrentCategoryIndex] = useState(0);
  const {
    API_ROUTE,
    locations,
    getLocations,
    tours,
    getAllCategories,
    categories,
    getRandomTours,
  } = useAuth();
  const [searchLocation, setSearchLocation] = useState(true);
  const [loading, setLoading] = useState(true);
  const [widthOutput, setWidthOutput] = useState(window.innerWidth);
  const isMounted = useRef(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await getLocations();
        await getRandomTours();
        await getAllCategories();
        setLoading(false);
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    };

    if (isMounted.current) {
      fetchData();
      isMounted.current = false;
    }
  }, []); // El segundo argumento [] indica que este efecto solo se ejecutará una vez (equivalente a componentDidMount)

  let list = useAsyncList({
    async load({ signal, filterText }) {
      let res = locations.filter((location) =>
        location.name.toLowerCase().includes(filterText.toLowerCase())
      );

      if (res.length > 10) {
        res = res.slice(0, 10);
      }
      //let json = await res.json();
      setSearchLocation(filterText)
      return {
        items: res,
      };
    },
  });

  const [currentPage, setCurrentPage] = React.useState(1);

  const shuffledTours = [...tours].sort(() => Math.random() - 0.5);
  const favoriteTours = [...tours]
    .sort((a, b) => b.favorites - a.favorites)
    .slice(0, 4);

  const handleResize = () => {
    setWidthOutput(window.innerWidth);
  };

  const itemsPerPage = 10; // Cantidad de tours por página

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div>
      {loading ? (
        <LoadingPage />
      ) : (
        <>
          <Header />

          <div className=" relative flex flex-col justify-center">
            <img
              alt="Card background"
              className="z-0 top-0 w-full  object-fit absolute h-[700px]"
              src="/images/background2.png"
            />
            <section className=" flex justify-center ">
              <div className=" z-10 top-1 flex flex-col !items-start gap-8 w-full">
                <div className="flex flex-col lg:flex-row gap-8 py-20 px-8 self-center">
                  <h1 className="text-center py-10 text-[#B185A8] font-bold text-[36px] w-full lg:text-[52px]">
                    ¡Elegí tu tour guiado!
                  </h1>

                  <div className="flex flex-row gap-2 self-center bg-[#F9F3DB] p-2 rounded-[12px] ">
                    <div className="flex flex-col gap-2">
                      <Autocomplete
                        className="max-w-xs"
                        isRequired
                        onSelectionChange={setSearchLocation}
                        inputValue={list.filterText}
                        isLoading={list.isLoading}
                        items={list.items}
                        label="Seleccione una ubicacion"
                        placeholder="Escriba la ubicacion..."
                        color="warning"
                        onInputChange={list.setFilterText}
                      >
                        {(item) => (
                          <AutocompleteItem key={item.id} value={item.id}>
                            {item.name}
                          </AutocompleteItem>
                        )}
                      </Autocomplete>

                      <div className="hidden flex flex-row gap-2">
                        <Input
                          label="Fecha"
                          type="date"
                          placeholder="¿En qué fecha?"
                          className="max-w-xs w-[130px]"
                        />
                        <Input
                          label="Fecha"
                          type="date"
                          placeholder="¿En qué fecha?"
                          className="max-w-xs w-[130px]"
                        />
                      </div>
                    </div>
                    <Button
                      isIconOnly
                      className="bg-[#E06A00] text-white  w-18"
                      onClick={()=>
                        (window.location.href = `/buscar/loc=${searchLocation}`) 
                      }
                    >
                      <SearchIcon />
                    </Button>
                  </div>
                </div>
                <img
                  src="/images/Frame-Decor-Img.png"
                  apxlt="Tipo de Tour"
                  className="rounded-[30px] object-fit bg-transparent self-center pt-4 w-4/5 max-w-[768px]"
                />
                <div className="flex flex-col self-center  p-3 rounded-[12px] gap-2 w-full">
                  <h2 className=" bg-[#B185A8] text-center w-3/5 py-2 mx-auto rounded-[16px] text-[18px] text-white my-8 max-w-[400px] font-semibold">
                    ¡Elegí tu tipo de tour!
                  </h2>
                  <div className="flex flex-row self-center">
                    <button
                      className="text-[#B185A8] font-bold"
                      onClick={() =>
                        setCurrentCategoryIndex(
                          (currentCategoryIndex - 1 + categories.length) %
                            categories.length
                        )
                      }
                    >
                      {"<"}
                    </button>
                    <div
                      className="flex flex-row gap-6 px-3 py-4 text-[#B185A8] text-center self-align"
                      style={{ transition: "transform 2s ease" }}
                    >
                      {Array.from({
                        length:
                          widthOutput >= 912 && categories.length >= 4
                            ? 4
                            : widthOutput < 912 && widthOutput > 712
                            ? 3
                            : widthOutput <= 712
                            ? 2
                            : 1,
                      }).map((_, index) => {
                        const categoryIndex =
                          (currentCategoryIndex + index) % categories.length;
                        return (
                          <CardCategory
                            key={categories[categoryIndex]?.id}
                            title={categories[categoryIndex]?.title}
                            image={categories[categoryIndex]?.image}
                          />
                        );
                      })}
                    </div>
                    <button
                      className="text-[#B185A8] font-bold"
                      onClick={() =>
                        setCurrentCategoryIndex(
                          (currentCategoryIndex + 1) % categories.length
                        )
                      }
                    >
                      {">"}
                    </button>
                  </div>
                </div>
              </div>
            </section>

            <section>
              <img
                src="/images/Recomendaciones.png"
                alt="Logo de la empresa"
                className="object-fit w-screen max-w-[1432px] pr-4 py-8 text-center"
              />
            </section>

            <div className="flex flex-row flex-wrap gap-8 max-w-[800px] self-center justify-center">
              {favoriteTours.map((tour) => (
                <CardFavorite
                  key={tour.id}
                  id={tour.id}
                  title={tour.name}
                  price={tour.price}
                  location={tour.location.name}
                  rating={tour.rating}
                  images={tour.images[0]}
                />
              ))}
            </div>
            <h2 className="bg-[#E06A00] text-center w-3/5 py-2 mx-auto rounded-[16px] text-[12px] text-white my-8">
              Todos nuestros tours
            </h2>
            <div className="flex flex-row flex-wrap gap-8 justify-center  max-w-[1024px] min-h-[500px] self-center items-center">
              {tours &&
                shuffledTours
                  .slice(
                    (currentPage - 1) * itemsPerPage,
                    currentPage * itemsPerPage
                  )
                  .map((tour) => (
                    <Card
                      key={tour.id}
                      id={tour.id}
                      title={tour.name}
                      price={tour.price}
                      location={tour.location.name}
                      rating={tour.rating}
                      images={tour.images[0]}
                    />
                  ))}
            </div>

            <Pagination
              className="py-10 self-center"
              classNames={{
                cursor: "bg-[#B185A8]   font-bold",
              }}
              showControls
              total={Math.ceil(tours.length / itemsPerPage)}
              page={currentPage}
              onChange={setCurrentPage}
            />
          </div>

          {open && (
            <div dir="rtl" className="p-0 m-0 h-[0px] bottom-0 z-[100] sticky ">
              <div className="p-0 m-0 h-[0px] -top-[150px] right-[20px] absolute ">
                <button onClick={() => setOpen(false)} className="p-0 m-0">
                  <CloseIcon />
                </button>

                <a
                  href="https://wa.me/1112312312"
                  className="h-[100px] object-bottom"
                >
                  <img
                    src="/images/WhatsApp_icon.png"
                    className=" h-[100px] object-bottom"
                  />
                </a>
              </div>
            </div>
          )}

          {<Footer />}
        </>
      )}
    </div>
  );
};

export default Home;
