import React from 'react'
import Header from '../../Components/Header/Header'
import Footer from '../../Components/Footer/Footer';
const PanelAdmin = () => {
  return (
    <div className='h-screen flex flex-col justify-between'>
        <Header/>
        <h1>Panel Admin</h1>
        <Footer/>
    </div>
  )
}

export default PanelAdmin