import React ,{useRef} from "react";
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import { useNavigate, useParams, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import Carousel from "../../Components/Carousel/Carousel";
import { useAuth } from "../../Context/AuthContext";
import { useForm } from "react-hook-form";
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  useDisclosure,
  Checkbox,
  Input,
  Link,
} from "@nextui-org/react";
import { Select, SelectItem } from "@nextui-org/react";
import { Chip } from "@nextui-org/react";
import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "react-share";
import {
  Dropdown,
  DropdownTrigger,
  DropdownMenu,
  DropdownSection,
  DropdownItem,
} from "@nextui-org/react";
import { Helmet } from "react-helmet";

const times = [
  { time: "06:00", key: "1" },
  { time: "07:00", key: "3" },
  { time: "08:00", key: "5" },
  { time: "09:00", key: "7" },
  { time: "10:00", key: "9" },
  { time: "11:00", key: "11" },
  { time: "12:00", key: "13" },
  { time: "13:00", key: "15" },
  { time: "14:00", key: "17" },
  { time: "15:00", key: "19" },
  { time: "16:00", key: "21" },
  { time: "17:00", key: "23" },
  { time: "18:00", key: "25" },
  { time: "19:00", key: "27" },
  { time: "20:00", key: "29" },
  { time: "21:00", key: "31" },
  { time: "22:00", key: "33" },
  { time: "23:00", key: "35" },
  { time: "24:00", key: "37" },
];

const DetalleProducto = () => {
  const [reserved, setReserved] = React.useState([]);
  const [appointments, setAppointments] = React.useState([]);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const { API_ROUTE, ROUTE } = useAuth();
  const { id } = useParams();
  const { currentUser, token, tours, getOneTour } = useAuth();
  const [loading, setLoading] = useState(true);
  const [reserveDetail, setReservedDetail] = useState(false);
  const [checkForm, setCheckForm] = useState();
  const [status, setStatus] = useState(true);
  const location = useLocation();
  const navigate = useNavigate();
  const shareUrl = ROUTE + location.pathname;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
  });

  const reserveSubmit = async () => {
    const fechaObjeto = new Date(checkForm.date);

    // Obtener componentes de fecha (día, mes, año)
    const dia = fechaObjeto.getDate();
    const mes = fechaObjeto.getMonth() + 1; // Se suma 1 porque los meses comienzan desde 0
    const año = fechaObjeto.getFullYear();
  
    // Formatear la fecha como "DD-MM-YYYY"
    const fechaFormateada = `${dia < 10 ? '0' : ''}${dia}-${mes < 10 ? '0' : ''}${mes}-${año}`;
  
    checkForm.date = fechaFormateada;

    try {
      console.log(checkForm);
      const response = await fetch(API_ROUTE + "/appointments", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(checkForm),
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setStatus("OK");
        window.location.href = `/confirmacion/${data.id}`;
        console.log("OK----------------------------" )
        payment(data);
      } else {
        setStatus("FAIL");
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } finally {
      //onOpen(true)
    }
  };

  const reserveCheck = async (form) => {
    form.tourId = parseInt(id);
    form.userId = currentUser.id;
    form.guideId = tour.id;
    setCheckForm(form);
    onOpen(false);
    setReservedDetail(true);
  };

  const goBack = () => {
    navigate(-1);
  };

  const openReserve = () => {
    setStatus(null);
    if (currentUser) {
      onOpen();
    } else {
      window.location.href = "/login";
    }
  };

  const reservedDate = (date) => {
    tourAppointments();
    console.log(appointments);
    const filteredData = appointments.filter(
      (appointment) => appointment.date == date
    );
    let timesArray = [];
    filteredData.forEach((appointment) => {
      const startTime = parseInt(appointment.time.substring(0, 5));
      const durationInHours = tour.durationInHours; // Se asume 1 hora si la duración no está definida

      for (let i = startTime; i < startTime + durationInHours + 1; i++) {
        timesArray.push(i.toString().padStart(2, "0") + ":00");
      }
    });
    setReserved(timesArray);
    console.log(reserved);
    console.log(timesArray);
  };

  useEffect(() => {
    console.log(reserved);
  }, [reserved]);

  const tourAppointments = async () => {
    try {
      const response = await fetch(
        API_ROUTE + `/appointments/by-tour-id?tourId=${id}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.ok) {
        const data = await response.json();
        const appointmentsArray = Array.isArray(data) ? data : [data];
        setAppointments(appointmentsArray);

        //setOk(true)
      } else {
        //setFail(true)
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } finally {
      //onOpen(true)
    }
  };

  const isMounted = useRef(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await getOneTour(id);
        console.log(tours)
        setLoading(false);
      } catch (error) {
        console.error(error);
        setLoading(false);
      }
    };

    if (isMounted.current) {
      fetchData();
      isMounted.current = false;
    }
  }, []); // El segundo argumento [] indica que este efecto solo se ejecutará una vez (equivalente a componentDidMount)

  return (
    <div>
      <Helmet>
        <meta property="og:title" content={tours?.name} />
        <meta property="og:description" content={tours?.description} />

        <meta property="og:url" content={shareUrl} />
        <meta property="og:type" content="website" />
        {/* Agrega otras metaetiquetas según sea necesario */}
      </Helmet>
      <Header />

      {loading ? (
        <ul>
          <li>
            <div>Loading ...</div>
          </li>
        </ul>
      ) : (
        <div className="cardDetail">
          <div className="overflow-visible flex items-center space-x-2 text-base mx-auto px-8 py-10 justify-between">
            <h1 className="flex-initial font-montserrat text-[32px] font-bold ">
              {tours?.name}
            </h1>
            <h4 className=" flex-none basis-3 font-montserrat text-[24px] text-[#FEBC14] font-bold text-center">
              {tours?.rating}★
            </h4>
            <div className="flex-initial flex space-x-2 mx-auto flex-nowrap">
              <button
                className="flex flex-1 justify-item-center font-montserrat text-[24px] font-light"
                onClick={goBack}
              >
                <img
                  src="/images/angulo-izquierdo.svg"
                  alt="Back late"
                  className="justify-item-center flex text-94 w-14"
                />
                <div className="flex flex-1 pt-4 text-[36px]"> Atrás </div>
              </button>
            </div>
          </div>
          <div className="flex px-8 justify-between ">
            <div className="flex">
              <img
                src="/images/location_1.svg"
                alt="location number 1"
                className="w-7"
              />
              <h3 className="pl-3.5 font-montserrat text-[24px] font-light">
                {tours?.location.name}
              </h3>
            </div>
            <h2 className="text-[#E06A00] text-[30px] font-semibold">
              ${tours?.price}
            </h2>
          </div>
          {tours.images && (
            <div className="flex justify-center ml-4 mt-10">
              <img
                alt="Card background"
                radius="none"
                className=" w-[20rem] self-center m-20 "
                src={tours?.images[0]}
                width="100%"
              />
            </div>
          )}
          {tours.images && (
            <Carousel
              className="flex justify-center "
              images={[
                tours?.images[1],
                tours?.images[2],
                tours?.images[3],
                tours?.images[4],
              ]}
            />
          )}

          <div className="flex py-10 flex-row justify-between mx-8">
            <div>
              <h2 className="flex flex-initial text-[#E06A00] font-montserrat text-[30px] font-bold py-4 w-full">
                Descripción
              </h2>
              <div className="justify-item-center font-montserrat text-[18px] mx-3">
                {tours?.description}
              </div>
              <div>
                <h2 className="flex flex-initial text-[#E06A00] font-montserrat text-[30px] font-bold py-4 w-[100%]">
                  Atributos
                </h2>
                <div className="flex mx-12">
                  <div className="flex">
                    <ul>
                      <li>
                        <div className="flex">
                          <img
                            src="/images/atri_1.svg"
                            alt=""
                            className="w-[2.2rem]"
                          />
                          <h3 className="mt-2 py-10px px-5 font-montserrat text-[16px] font-bold">
                            Cantidad max. personas:{" "}
                          </h3>
                          <h3 className="text-[#E06A00] font-montserrat text-[30px] font-bold">
                            {tours?.category.id}
                          </h3>
                        </div>
                      </li>
                      <li>
                        <div className="flex mt-8 content-start">
                          <img
                            src="/images/atri_7.svg"
                            alt=""
                            className="w-[2rem]"
                          />
                          <h3 className="px-10  font-montserrat text-[16px] font-bold">
                            Duración:{" "}
                          </h3>
                          <h3 className="text-[#E06A00] font-montserrat text-[40px] font-bold">
                            {tours?.durationInHours}
                          </h3>
                        </div>
                      </li>
                      <li>
                        <div className="flex mt-8">
                          <img
                            src="/images/atri_2.svg"
                            alt=""
                            className="w-[2rem]"
                          />
                          <h3 className="px-5"> Medio de pago:</h3>
                          <div className="flex justify-items-center">
                            <img
                              src="/images/atri_4.svg"
                              alt=""
                              className="w-[2.2rem] flex-1 mx-2"
                            />
                            <img
                              src="/images/atri_5.svg"
                              alt=""
                              className="w-[2.2rem] flex-1 mx-2"
                            />
                            <img
                              src="/images/atri_3.svg"
                              alt=""
                              className="w-[2.2rem] flex-1 mx-2"
                            />
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>

                  <div className="flex-align ml-[24rem] ">
                    <h2 className="flex flex-col font-montserrat text-[16px] font-bold text-[#002000] ">
                      {" "}
                      Guias disponibles:{" "}
                    </h2>
                    <div className="flex p-3">
                      <img
                        src="/images/person-profile.svg"
                        alt=""
                        className="w-[3.2rem] flex-1 mx-2"
                      />
                      <div className="flex flex-col gap-1">
                        <h3>{tours?.guide.firstName}</h3>
                        <h3>{tours?.guide.lastName}</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="flex-initial flex  m-20">
                <button
                    className=" flex-1 mx-auto  h-[4rem] bg-[#E06A00] rounded-3xl text-[#FFFFFF] font-montserrat text-[25px] font-bold  "
                    onClick={openReserve}
                  >
                    Reservar
                  </button>
                  <Dropdown>
                    <DropdownTrigger>
                      <Button isIconOnly>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke-width="1.5"
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
     
                            d="M7.217 10.907a2.25 2.25 0 100 2.186m0-2.186c.18.324.283.696.283 1.093s-.103.77-.283 1.093m0-2.186l9.566-5.314m-9.566 7.5l9.566 5.314m0 0a2.25 2.25 0 103.935 2.186 2.25 2.25 0 00-3.935-2.186zm0-12.814a2.25 2.25 0 103.933-2.185 2.25 2.25 0 00-3.933 2.185z"
                          />
                        </svg>
                      </Button>
                    </DropdownTrigger>
                    <DropdownMenu>
                      <DropdownItem
                        startContent={<WhatsappIcon size={16} round={true} />}
                      >
                        <WhatsappShareButton
                          url={ROUTE + location.pathname}
                          title={"Mira este tour... " + tours.name}
                        >
                          WhatsApp
                        </WhatsappShareButton>
                      </DropdownItem>
                      <DropdownItem
                        startContent={<TwitterIcon size={16} round={true} />}
                      >
                        <TwitterShareButton url={ROUTE + location.pathname}>
                          X
                        </TwitterShareButton>
                      </DropdownItem>
                      <DropdownItem
                        startContent={<TelegramIcon size={16} round={true} />}
                      >
                        <TelegramShareButton url={ROUTE + location.pathname}>
                          Telegram
                        </TelegramShareButton>
                      </DropdownItem>
                      <DropdownItem
                        startContent={<FacebookIcon size={16} round={true} />}
                      >
                        <FacebookShareButton url={ROUTE + location.pathname}>
                          Facebook
                        </FacebookShareButton>
                      </DropdownItem>
                      <DropdownItem
                        startContent={<EmailIcon size={16} round={true} />}
                      >
                        <EmailShareButton url={ROUTE + location.pathname}>
                          Email
                        </EmailShareButton>
                      </DropdownItem>
                    </DropdownMenu>
                  </Dropdown>


                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      <Modal isOpen={isOpen} onOpenChange={onOpenChange} placement="top-center">
        <ModalContent>
          {(onClose) => (
            <>
              <form onSubmit={handleSubmit(reserveCheck)}>
                <ModalHeader className="flex flex-col gap-1 text-[#06A77D] ">
                  Reserve su tour!
                </ModalHeader>
                <ModalBody>
                  <Input
                    onValueChange={reservedDate}
                    labelPlacement="outside"
                    type="date"
                    label="Fecha"
                    placeholder="Eliga su fecha"
                    variant="bordered"
                    {...register("date")}
                    color="warning"
                    classNames={{
                      label: "text-[#B185A8] ",
                      input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                      placeholder: "text-[#B185A8]",
                      renderValue: [
                        "text-[#B185A8]",
                        "bg-[#F9F3DB]",
                        "hover:bg-[#F9F3DB]",
                      ],
                      popover: [
                        "text-[#B185A8]",
                        "bg-[#F9F3DB]",
                        "hover:bg-[#F9F3DB]",
                      ],
                      value: ["text-[#B185A8]"],
                    }}
                  />
                  <Select
                    labelPlacement="outside"
                    color="warning"
                    label="Hora"
                    placeholder="Seleccione una hora"
                    disabledKeys={reserved}
                    className="max-w-xs"
                    {...register("time")}
                    classNames={{
                      input: ["text-[#B185A8]", "placeholder:text-[#B185A8]"],
                      placeholder: "text-[#B185A8]",
                      renderValue: ["text-[#B185A8]", "bg-[#F9F3DB]"],
                      popover: ["text-[#B185A8]", "bg-[#F9F3DB]"],
                      value: ["text-[#B185A8]"],
                    }}
                    listboxProps={{
                      itemClasses: {
                        base: [
                          "font-bold",
                          "data-[selectable=true]:focus:bg-[#F9F3DB]", //del item seleccionado
                          "data-[disabled=true]:text-[#000000]",
                          "data-[hover=true]:bg-[#F9F3DB]",
                        ],
                      },
                    }}
                  >
                    {times.map((time) => (
                      <SelectItem key={time.time} value={time.time}>
                        {time.time}
                      </SelectItem>
                    ))}
                  </Select>
                </ModalBody>
                <ModalFooter>
                  <Button
                    color="danger"
                    variant="flat"
                    onPress={onClose}
                    className="bg-[#06A77D] text-white font-semibold"
                  >
                    Cancelar
                  </Button>
                  <Button
                    color="primary"
                    type="submit"
                    className="bg-[#E06A00] text-white font-semibold"
                  >
                    Reservar
                  </Button>
                </ModalFooter>
              </form>
            </>
          )}
        </ModalContent>
      </Modal>

      <Modal
        isOpen={isOpen && reserveDetail}
        onOpenChange={() => {
          onOpenChange();
          setReservedDetail(false);
        }}
        placement="top-center"
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1 text-[#06A77D] ">
                Confirme sus datos
              </ModalHeader>
              <ModalBody className="flex flex-col gap-4">
                <div className="flex flex-col gap-1">
                  <h1 className="text-[#06A77D] font-semibold">Mis datos</h1>
                  <div className="flex flex-col">
                    <h2 className="font-bold">
                      Nombre:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {currentUser.firstName}
                      </p>
                    </h2>
                    <h2 className="font-bold">
                      Apellido:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {currentUser.lastName}
                      </p>{" "}
                    </h2>
                    <h2 className="font-bold">
                      Mail:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {currentUser.email}
                      </p>{" "}
                    </h2>
                  </div>
                </div>

                <div className="flex flex-col gap-1">
                  <h1 className="text-[#06A77D] font-semibold">
                    Detalles del tour
                  </h1>
                  <div className="flex flex-col">
                    <h2 className="font-bold">
                      Nombre:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {tour.name}
                      </p>{" "}
                    </h2>
                    <h2 className="font-bold">
                      Ubicacion:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {tour.location.name}
                      </p>{" "}
                    </h2>
                    <h2 className="font-bold">
                      Duracion:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {tour.durationInHours} hs
                      </p>{" "}
                    </h2>
                    <h2 className="font-bold">
                      Precio:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {tour.price}
                      </p>{" "}
                    </h2>
                  </div>
                </div>

                <div className="flex flex-col gap-1">
                  <h1 className="text-[#06A77D] font-semibold">Mi reserva</h1>
                  <div className="flex flex-col">
                    <h2 className="font-bold">
                      Fecha:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {checkForm.date}
                      </p>
                    </h2>

                    <h2 className="font-bold">
                      Hora:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {checkForm.time}
                      </p>
                    </h2>
                    <h2 className="font-bold">
                      Mail:{" "}
                      <p className="inline text-[#E06A00] font-semibold">
                        {currentUser.email}
                      </p>
                    </h2>
                  </div>
                </div>
                {status == "FAIL" && (
                  <Chip
                    color="danger"
                    variant="bordered"
                    className="self-center"
                  >
                    Ups!, hubo un problema. Vuelva a intentarlo
                  </Chip>
                )}
                {status == "OK" && (
                  <Chip
                    color="success"
                    variant="bordered"
                    className="self-center"
                  >
                    Ya tiene su reservacion!
                  </Chip>
                )}
              </ModalBody>
              <ModalFooter className="flex justify-center">
                <Button
                  color="primary"
                  onPress={() => setReservedDetail(false)}
                  className="bg-[#06A77D] text-white font-semibold"
                >
                  Atras
                </Button>
                <Button
                  color="primary"
                  onPress={() => reserveSubmit()}
                  className="bg-[#E06A00] text-white font-semibold"
                >
                  Reservar
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
      <Footer />
    </div>
  );
};

export default DetalleProducto;
