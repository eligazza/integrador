import React, { useContext, useState, useEffect } from "react"
import { uploadCloudinary } from "../uploadCloudinary";
import axios from "axios";
import LoadingPage from "../Components/Loading/LoadingPage";
import { useDisclosure } from "@nextui-org/react";
import SuccessAlert from "../Components/Alerts/Success";
import FailAlert from "../Components/Alerts/Fail";

const AuthContext = React.createContext()

export function useAuth() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }) {
  const [categories, setCategories] = useState([]);
  const [locations, setLocations] = useState([]);
  const [tours, setTours] = useState([]);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [fail, setFail] = useState(false);
  const [success, setSuccess] = useState(false);
  const [sendingData, setSendingData] = useState([]);
  const currentUser = JSON.parse(localStorage.getItem("user"));
  const token = JSON.parse(localStorage.getItem("token"));
  //const API_ROUTE='http://localhost:11000';
  //const ROUTE = "http://localhost:5173"
  const ROUTE = "http://www.guider.com.ar";
  const API_ROUTE="http://api.guider.com.ar:11000"
  

  const [loading, setLoading] = useState(true)
  //setTimeout(() => { setLoading(false) }, 2000);

  const header = {
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`
    }
  };

  const checkAndRefresh = () => {
    const storedTime = localStorage.getItem("time");
    const currentTime = new Date().getTime();

    if (storedTime && currentTime - parseInt(storedTime, 10) > 6 * 60 * 1000) {
      localStorage.clear();
      window.location.reload();
    }
  };

  //Metodos de tours

  const getAllTours = async () => {
    await axios.get(API_ROUTE + "/tours")
      .then(response => {
        //console.log(response.data);
        setTours(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };

  const getRandomTours = async () => {
    await axios.get(API_ROUTE + "/tours/random")
      .then(response => {
        setTours(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };

  const postOneTour = async (form, images) => {
    setFail(false)
    setSuccess(false)
    try {
      form.images = [];
      for (let i = 0; i < images.length; i++) {
        const data = await uploadCloudinary(images[i]);
        form.images.push(data);
      }
      await axios.post(API_ROUTE + "/tours", form, header)
        .then(response => {
          console.log(response.data);
          console.log("Subido!")
          setSuccess(true)
        })
        .catch(error => {
          console.error(error.response.data);
          setFail(true)
        }).finally(onOpen(true));
    } catch (error) {
      console.error(error);
    } finally {
      //onOpen(true)
    }
  }
  const deleteOneTour = async (tourId) => {
    setFail(false)
    setSuccess(false)
    await axios.delete(API_ROUTE + `/tours/${tourId}`, header)
      .then(response => {
        console.log(response.data);
        getMyTours()
        setSuccess(true)
      })
      .catch(error => {
        console.error(error.response.data);
        setFail(true)
      }).finally(onOpen(true));
  };

  const getOneTour = async (id) => {
    await axios.get(API_ROUTE + `/tours/${id}`)
      .then(response => {
        setTours(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };

  const putOneTour = () => {

  };

  const getMyTours = async () => {
    await axios.get(API_ROUTE + `/tours/by-guide-id?guideId=${currentUser.id}`,header)
      .then(response => {
        console.log(response.data);
        setTours(response.data)
      })
      .catch(error => {
        console.error(error.response.data);
      })
  };



  //Categories

  const getAllCategories = async () => {
    await axios.get(API_ROUTE + "/categories")
      .then(response => {
        //console.log(response.data);
        setCategories(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };

  const postOneCategory = async (form) => {
    setFail(false)
    setSuccess(false)
    await axios.post(API_ROUTE + "/categories",form)
      .then(response => {
        console.log(response.data);
        setSuccess(true)
        getAllCategories()
      })
      .catch(error => {
        console.error(error.response.data);
        setFail(true)
      }).finally(onOpen(true));
  };

  const deleteOneCategory = async () => {
    await axios.get(API_ROUTE + "/categories")
      .then(response => {
        //console.log(response.data);
        setCategories(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };

  const getOneCategory = async () => {
    await axios.get(API_ROUTE + "/categories")
      .then(response => {
        //console.log(response.data);
        setCategories(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };

  const putOneCategory = async () => {
    await axios.get(API_ROUTE + "/categories")
      .then(response => {
        //console.log(response.data);
        setCategories(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };




  const getLocations = async () => {
    await axios.get(API_ROUTE + "/locations")
      .then(response => {
        //console.log(response.data);
        setLocations(response.data)
      })
      .catch(error => {
        console.error(error);
      });
  };






  React.useEffect(() => {
    const intervalId = setInterval(checkAndRefresh, 10000); // Verifica cada 10 segundos
    return () => clearInterval(intervalId); // Limpia el intervalo al desmontar el componente
  }, []);

  const value = {
    currentUser,
    token,
    API_ROUTE,
    categories,
    locations,
    ROUTE,
    getLocations,
    getAllTours,
    getRandomTours,
    getAllCategories,
    postOneTour,
    getOneTour,
    deleteOneTour,
    getMyTours,
    postOneCategory,
    tours,
  }

  return (
    <AuthContext.Provider value={value}>
      {loading ? (
        <>
          {children}
          <SuccessAlert
            isOpen={isOpen && success}
            onOpenChange={onOpenChange}
            onOpen={onOpen}
          />
          <FailAlert
            isOpen={isOpen && fail}
            onOpenChange={onOpenChange}
            onOpen={onOpen}
          />
        </>

      ) : (
        <LoadingPage />
      )}
    </AuthContext.Provider>
  )
}
