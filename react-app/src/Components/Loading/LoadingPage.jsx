import React from "react";
import {CircularProgress} from "@nextui-org/react";

const LoadingPage = ({id, title, price, location, rating, images}) => {
  return (
    <div className=" h-screen flex flex-col justify-center items-center">
    <img
    alt="Logo"
    src="/images/Logo.png"
    className="object-fit w-3/5"
    />

    <CircularProgress size="lg" aria-label="Loading..."/>
  </div>
  );
};

export default LoadingPage;
