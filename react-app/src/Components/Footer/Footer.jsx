import React from "react";
import { Link } from "react-router-dom";
import { HeartIcon } from "../../../public/icons.jsx";

const Footer = () => {
  return (
    <footer className="h-[65px] bg-[#F9F3DB] grid grid-cols-3 justify-items-center content-center">
      <Link to="/" className="justify-self-start  pl-4">
        <img
          src="/images/Logo.png"
          alt="Logo de la empresa"
          className="h-[45px] object-fit"
        />
      </Link>
      <div className="place-self-center flex gap-1">
        <span>Made with</span>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          fill="red"
          className={`w-6 h-6 `}
        >
          <path
            d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
          />
        </svg>
        <span> by EpicStudio</span>
      </div>

      <div className="justify-self-end place-self-center pr-4">
        <div className="flex gap-3">
          <img
            src="/images/instagram.png"
            alt="Logo de la empresa"
            className="h-[30px] object-fit"
          />
          <img
            src="/images/facebook.png"
            alt="Logo de la empresa"
            className="h-[30px] object-fit"
          />
          <img
            src="/images/gorjeo.png"
            alt="Logo de la empresa"
            className="h-[30px] object-fit"
          />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
