
import React from "react";
import { Card , CardFooter, Image } from "@nextui-org/react";
import { Link } from "react-router-dom";


const CardCategory = ({id, title, image}) => {
  return ( 
<Card isPressable isFooterBlurred className="w-48 h-80 col-span-12 sm:col-span-7" onPress={() => window.location.href =`buscar/cat=${title}`}>
      <Image
        removeWrapper
        alt="Relaxing app background"
        className="z-0 h-full object-cover"
        src={image}
      />
      <CardFooter className="justify-between before:bg-white/10 border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10">
        <h1 className="font-semibold text-white ">{title}</h1>
      </CardFooter>
    </Card>
  );
};

export default CardCategory;
