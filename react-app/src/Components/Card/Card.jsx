import React, { useState } from "react";
import {
  Card as Cards,
  CardBody,
  Image,
  CardFooter,
  Button,
  Chip
} from "@nextui-org/react";
import { useAuth } from "../../Context/AuthContext";


//favs/add?userId=1&tourId=1
const Card = ({ id, title, price, location, rating, images}) => {
  const {API_ROUTE,currentUser,token} = useAuth()
  const [favorites, setFavorites] = useState(
    JSON.parse(localStorage.getItem("favorites")) || []
  );
  const [tags, setTags] = useState([]  );
  const getTags = async () => {
    try {
      const response = await fetch(API_ROUTE + `/tags/by-tour-is?tourId=${id}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.ok) {
        const data = await response.json();
        setTags(data); // Actualizar el estado de 'favorites'
      } else {
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } 
  };

  const addFav = async () => {
    try {
      const response = await fetch(API_ROUTE + `/favs/add?userId=${currentUser.id}&tourId=${id}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.ok) {
        const data = await response.json();
        const updatedFavorites = data.map(item => item.id); // Extrae solo los valores de id
        localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
        setFavorites(updatedFavorites); // Actualizar el estado de 'favorites'
      } else {
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } 
  };

  const removeFav = async () => {
    try {
      const response = await fetch(API_ROUTE + `/favs/remove?userId=${currentUser.id}&tourId=${id}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.ok) {
        const data = await response.json();
        const updatedFavorites = data.map(item => item.id); // Extrae solo los valores de id
        localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
        setFavorites(updatedFavorites); // Actualizar el estado de 'favorites'
      } else {
        console.log(
          "Error al enviar la solicitud. Código de estado:",
          response.status
        );
      }
    } catch (error) {
      console.error(error);
    } 
  };

  const fav = async () => {
    if(favorites.includes(id)){
      removeFav();
    } else {
      addFav();
    }

  };


  return (
    <Cards
      className="w-56 h-52 bg-[#F9F3DB] rounded-[17px] justify-between flex"
      key={id}
    >


      <Image
        alt="Card background"
        radius="none"
        className="w-56 object-cover h-24 "
        src={images}
      />
      <Button isIconOnly variant="light" color="danger" aria-label="Like" className="absolute z-[10]  top-1 right-1" onClick={() =>fav()}>
        <svg xmlns="http://www.w3.org/2000/svg"
          fill={favorites.includes(id)? ("red") : ("none")} viewBox="0 0 24 24" stroke="red" className="w-[30px] text-red bg-red stroke-1">
          <path  d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z" />
        </svg>
      </Button>
      <CardBody className="overflow-visible py-[8px] px-[10px] flex">
        <div className="flex flex-row w-full justify-between items-center">
          <small className="text-default-500 text-[12px]">{location}</small>
          <p className="text-[14px] text-[#FEBC14] font-bold text-center">
            {rating}★{" "}
          </p>
        </div>
        <h4 className="font-bold text-[14px] w-fill self-start">{title}</h4>
        <div className = "flex flex-row gap-2">
        {tags && tags.map((tag) => (
         <Chip key={tag.id} color="danger" variant="bordered">{tag.title}</Chip> //no, oker
         
         ))}
        </div>

      </CardBody>
      <CardFooter className="self-end flex justify-between">
        <p className="italic font-bold text-[#E06A00] text-[11px]">
          {" "}
          {price} $
        </p>
        <Button className="bg-[#E06A00] text-white font-medium h-[22px] " onPress={() => (window.location.href = `producto/${id}`)}>
          Reservar
        </Button>
      </CardFooter>
    </Cards>
  );
};

export default Card;

/*
    <Cards isPressable className="w-40 h-52 bg-[#F9F3DB] rounded-[17px]" onPress={() => window.location.href =`producto/${id}`} key={id}>
      <Link to={`/producto/${id}`}>
      <Image
        alt="Card background"
        radius="none"
        className="w-40 object-cover h-[98px]  "
        src={images}
        width="100%"
      />
      <CardBody className="overflow-visible py-[8px] px-[10px] flex justify-between">
        <div className="flex flex-row w-full justify-between items-start">
          <h4 className="font-bold text-[9px] w-[86px]">{title}</h4>
          <p className="italic font-bold text-[#E06A00] text-[11px]">{price}</p>
        </div>
        <small className="text-default-500">{location}</small>
        <p className="text-[14px] text-[#FEBC14] font-bold text-center">{rating} ★ </p>
      
      </CardBody>
      </Link>
    </Cards>

*/
