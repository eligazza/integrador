import React from "react";
import { CheckIcon } from "../../utils/icons";
import {
    Modal,
    ModalContent,
  } from "@nextui-org/react";

const SuccessAlert = ({ isOpen, onOpen, onOpenChange }) => {
    
  return (
    <Modal
    backdrop="transparent"
    isOpen={isOpen}
    onOpenChange={onOpenChange}
    placement="top"
    radius="none"
    isDismissable
    classNames={{
      body: "py-6",
      backdrop: "bg-[#292f46]/50 backdrop-opacity-40",
      base: "border-[#292f46] bg-[#28a745] dark:bg-[#19172c] text-[#a8b0d3]",
    }}
  >
    <ModalContent>
      {(onClose) => (
        <>
            <div className="flex ">
              <div className="bg-[#28a745] h-fill  w-1/12 text-white font-black border border-[#28a745] flex items-center justify-center">
               <CheckIcon className="p-auto" />
              </div>
              <div className="bg-[#e5f4e8] w-11/12 border border-[#28a745] text-[#28a745] p-2">
                <h1>Okay!</h1>
                <p className="text-sm">Todo paso sin problemas</p>
              </div>
            </div>
        </>
      )}
    </ModalContent>
  </Modal>
  );
};

export default SuccessAlert;
