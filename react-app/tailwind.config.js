const { nextui } = require("@nextui-org/react");

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    fontFamily: {
       sans: '"Gilroy", "Gilroy", "Gilroy", sans-serif',
       poppins: ['Poppins', 'sans-serif'],'poppins': ['sans-serif', 'sans-serif']
    }
  },
  darkMode: "class",
  plugins: [nextui()]
}