package com.guider.dtos.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppointmentDtoRes {

    private Long id;
    private TourDtoRes tour;
    private UserDtoRes guide;
    private UserDtoRes user;
    private RatingDtoRes rating;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;
    @DateTimeFormat(pattern = "HH:mm")
    private LocalTime time;


}




