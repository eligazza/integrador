package com.guider.dtos.response;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TagDtoRes {
    private Long id;
    private String title;
    private String color;
    private String description;
    private String image;
}
