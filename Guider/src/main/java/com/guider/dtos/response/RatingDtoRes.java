package com.guider.dtos.response;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RatingDtoRes {

    private Long id;
    private AppointmentDtoRes appointment;
    private Long tourId;
    private String userName;
    private Integer stars;
    private String review;

}
