package com.guider.dtos.response;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LocationDtoRes {
    private Long id;
    private String name;
    private double latitude;
    private double longitude;
}
