package com.guider.dtos.response;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CategoryDtoRes {

    private Long id;
    private String title;
    private String description;
    private String image;

}
