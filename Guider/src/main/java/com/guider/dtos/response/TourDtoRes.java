package com.guider.dtos.response;

import com.guider.persistence.entity.Location;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class TourDtoRes {

    private Long id;
    private String name;
    private Location location;
    private UserDtoRes guide;
    private Integer durationInHours;
    private Double price;
    private String description;
    private Double rating;
    private CategoryDtoRes category;
    private List<String> images;
    private List<Long> tags;

}
