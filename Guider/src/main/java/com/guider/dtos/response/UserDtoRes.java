package com.guider.dtos.response;

import com.guider.persistence.entity.Role;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class UserDtoRes {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    private List<Long> favourites;

}
