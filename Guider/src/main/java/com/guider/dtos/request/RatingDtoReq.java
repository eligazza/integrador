package com.guider.dtos.request;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RatingDtoReq {

    private Long id;
    private Long appointmentId;
    private Integer stars;
    private String review;

}
