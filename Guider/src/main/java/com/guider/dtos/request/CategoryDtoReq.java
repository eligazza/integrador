package com.guider.dtos.request;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CategoryDtoReq {

    private Long id;
    private String title;
    private String description;
    private String image;

}
