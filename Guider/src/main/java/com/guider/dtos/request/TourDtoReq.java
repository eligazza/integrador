package com.guider.dtos.request;

import com.guider.persistence.entity.Location;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class TourDtoReq {

    private Long id;
    private String name;
    private Long locationId;
    private Long guideId;
    private Integer durationInHours;
    private Double price;
    private String description;
    private Double rating;
    private Long categoryId;
    private List<String> images;
    private List<Long> tags;

}
