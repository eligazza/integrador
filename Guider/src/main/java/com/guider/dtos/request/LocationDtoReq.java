package com.guider.dtos.request;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LocationDtoReq {
    private Long id;
    private String name;
    private double latitude;
    private double longitude;
}
