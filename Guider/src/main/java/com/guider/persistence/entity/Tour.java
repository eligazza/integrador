package com.guider.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tours")
public class Tour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToOne @JoinColumn(name = "locationId", referencedColumnName = "id")
    private Location location;
    private Integer durationInHours;
    private Double price;
    @Column (length = 5000)
    private String description;
    private Double rating;
    @Column(columnDefinition = "varbinary(2048)")
    private List<String> images;
    @ManyToOne @JoinColumn(name = "userId", referencedColumnName = "id", nullable = false)
    private User guide;
    @ManyToOne @JoinColumn(name = "categoryId", referencedColumnName = "id", nullable = false)
    private Category category;
    private List<Long> tags;

    // Constructor
    public Tour(String name, Location location, Integer durationInHours, Double price, String description, List<String> images, User guide, Category category, List<Long> tags) {
        this.name = name;
        this.location = location;
        this.durationInHours = durationInHours;
        this.price = price;
        this.description = description;
        this.images = images;
        this.guide = guide;
        this.category = category;
        this.tags = tags;
    }
}





