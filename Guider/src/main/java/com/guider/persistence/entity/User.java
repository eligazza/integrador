package com.guider.persistence.entity;

import java.io.Serializable;
import java.util.*;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jakarta.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotEmpty
    @NotNull
    private String firstName;

    @Column
    @NotEmpty
    @NotNull
    private String lastName;

    @Email
    @Column(unique = true)
    @NotEmpty
    @NotNull
    private String email;

    @Column
    @NotEmpty
    @NotNull
    private String password;

    @Column
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column
    private List<Long> favourites;

}
