package com.guider.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne @JoinColumn(name = "appointment_id", referencedColumnName = "id", unique = true)
    private Appointment appointment;
    private Long tourId;
    private String userName;
    private Integer stars;
    @Column (length = 5000)
    private String review;

}
