package com.guider.persistence.entity;

public enum Role {
    USER,
    GUIDER,
    ADMIN
}
