package com.guider.persistence.repository;

import com.guider.persistence.entity.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface IAppointmentRepository extends JpaRepository<Appointment, Long> {

    @Query("SELECT a FROM Appointment a WHERE a.guide.id = ?1 ORDER BY a.date DESC")
    List<Appointment> findByGuideId(Long id);

    @Query("SELECT a FROM Appointment a WHERE a.user.id = ?1 ORDER BY a.date DESC")
    List<Appointment> findByUserId(Long id);

    @Query("SELECT a FROM Appointment a WHERE a.tour.id = ?1 ORDER BY a.date DESC")
    List<Appointment> findByTourId(Long id);

    @Query("SELECT a FROM Appointment a WHERE a.date = ?1 ORDER BY a.date DESC")
    List<Appointment> findByDate(LocalDate date);
}
