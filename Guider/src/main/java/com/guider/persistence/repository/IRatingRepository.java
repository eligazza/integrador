package com.guider.persistence.repository;

import com.guider.persistence.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IRatingRepository extends JpaRepository<Rating, Long> {

    //@Query("SELECT r FROM Rating r WHERE r.tourId = ?1")
    List<Rating> findByTourId(Long id);

    Optional<Rating> findByAppointmentId(Long id);

}
