package com.guider.persistence.repository;

import com.guider.persistence.entity.Tour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ITourRepository extends JpaRepository<Tour, Long> {

    Optional<Tour> findByName(String name);

    List<Tour> findByLocationName(String locationName);

    @Query("SELECT t FROM Tour t " +
            "WHERE FUNCTION('ST_Distance', FUNCTION('POINT', t.location.longitude, t.location.latitude), " +
            "FUNCTION('POINT', :longitude, :latitude)) < :distance") // distance in meters
    List<Tour> findByLocationCoordinatesNear(double latitude, double longitude, double distance);

    @Query("SELECT t FROM Tour t WHERE t.guide.id = ?1")
    List<Tour> findByGuideId(Long id);

    @Query("SELECT t FROM Tour t WHERE t.category.id = ?1")
    List<Tour> findByCategoryId(Long id);

    @Query("SELECT t FROM Tour t ORDER BY FUNCTION('RAND')")
    List<Tour> getRandomTours();
}
