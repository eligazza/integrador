package com.guider.service;


import com.guider.dtos.request.SignUpRequest;
import com.guider.dtos.request.LoginRequest;
import com.guider.dtos.response.JwtAuthenticationResponse;
import com.guider.exceptions.DuplicatedException;
import com.guider.exceptions.NotFoundException;
import com.guider.exceptions.UserNotExistsException;
import jakarta.mail.MessagingException;

public interface IAuthenticationService {
    JwtAuthenticationResponse signup(SignUpRequest request) throws MessagingException, DuplicatedException;

    JwtAuthenticationResponse login(LoginRequest request) throws NotFoundException, UserNotExistsException;
}
