package com.guider.service;

import com.guider.exceptions.InvalidTokenException;
import org.springframework.security.core.userdetails.UserDetails;

public interface IJwtService {

    String generateToken(String email);

    boolean validateToken(String token) throws InvalidTokenException;
}
