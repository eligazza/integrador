package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.guider.dtos.response.LocationDtoRes;
import com.guider.exceptions.NoContentException;
import com.guider.exceptions.NotFoundException;
import com.guider.persistence.entity.Location;
import com.guider.persistence.repository.ILocationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing locations.
 */

@Service
public class LocationService {

    // Attributes
    private final ILocationRepository locationRepository;
    private final ObjectMapper mapper;

    /**
     * Constructor for LocationService.
     * @param locationRepository The repository for managing locations.
     * @param mapper             The ObjectMapper for converting entities to DTOs.
     */
    public LocationService(ILocationRepository locationRepository, ObjectMapper mapper) {
        this.locationRepository = locationRepository;
        this.mapper = mapper;
    }

    /**
     * Retrieves all locations.
     * @return A list of LocationDtoRes objects representing all locations.
     * @throws NoContentException Thrown when no locations are found in the database.
     */
    public List<LocationDtoRes> findAll() throws NoContentException {
        List<Location> allLocations = locationRepository.findAll();
        if (allLocations.isEmpty()) {
            throw new NoContentException("No appointments in the database");
        } else {
            return mapToLocationDto(allLocations);
        }
    }

    /**
     * Retrieves a location by its ID.
     * @param id The ID of the location to retrieve.
     * @return The LocationDtoRes object representing the found location.
     * @throws NotFoundException Thrown when no location is found with the specified ID.
     */
    public LocationDtoRes findById(Long id) throws NotFoundException {
        return locationRepository.findById(id)
                .map(this::convertToLocationDto)
                .orElseThrow(() -> new NotFoundException("There is no location with id " + id));
    }

    // Utils
    /**
     * Maps a list of Location entities to a list of LocationDtoRes objects.
     * @param locations The list of Location entities to map.
     * @return A list of LocationDtoRes objects.
     */
    private List<LocationDtoRes> mapToLocationDto(List<Location> locations) {
        return locations.stream()
                .map(location -> mapper.convertValue(location, LocationDtoRes.class))
                .collect(Collectors.toList());
    }
    /**
     * Converts a Location entity to a LocationDtoRes object.
     * @param location The Location entity to convert.
     * @return The LocationDtoRes object.
     */
    private LocationDtoRes convertToLocationDto(Location location) {
        return mapper.convertValue(location, LocationDtoRes.class);
    }
}
