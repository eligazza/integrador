package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.guider.dtos.request.CategoryDtoReq;
import com.guider.dtos.response.CategoryDtoRes;
import com.guider.exceptions.*;
import com.guider.persistence.entity.Category;
import com.guider.persistence.repository.ICategoryRepository;
import com.guider.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing categories.
 * This service provides CRUD operations for handling categories, including
 * finding, saving, updating, and deleting categories. It also includes utility
 * methods for checking duplicated titles and existence check.
 *
 * @author Elias Gazza
 * @version 1.0
 * @since 2023-12-04
 */

@Service
public class CategoryService implements IService<CategoryDtoReq, CategoryDtoRes> {

    // Attributes
    //private static final Logger LOG = LogManager.getLogger(TourService.class);
    private final ObjectMapper mapper;
    private final ICategoryRepository categoryRepository;

    /**
     * Constructor for CategoryService.
     *
     * @param mapper                  The ObjectMapper for object conversion.
     * @param categoryRepository      The repository for managing categories.
     */
    public CategoryService(ObjectMapper mapper, ICategoryRepository categoryRepository) {
        this.mapper = mapper;
        this.categoryRepository = categoryRepository;
        configureObjectMapper();
    }

    /**
     * Retrieves all categories from the database.
     * @return List of CategoryDtoRes representing categories.
     * @throws NoContentException: If there are no categories in the database.
     */
    public List<CategoryDtoRes> findAll() throws NoContentException {
        List<Category> allCategories = categoryRepository.findAll();
        if (allCategories.isEmpty()) {
            throw new NoContentException("No categories in the database");
        } else {
            return mapToCategoryDto(allCategories);
        }
    }

    /**
     * Finds a category by ID.
     * Parameters:
     * @param id The ID of the category to retrieve.
     * @return CategoryDtoRes representing the found category.
     */
    public CategoryDtoRes findById(Long id) throws NotFoundException, MissingArgumentException {
        return categoryRepository.findById(id)
                .map(this::convertToCategoryDto)
                .orElseThrow(() -> new NotFoundException("There is no category with id " + id));
    }

    /**
     * Saves a new category or updates an existing one.
     * @param categoryDtoReq The data for the category.
     * @return The saved CategoryDtoRes object.
     * @throws DuplicatedException If there is already a category with the same title.
     * @throws InvalidArgumentException If the request data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     * @throws NotFoundException If a referenced entity is not found.
     */
    public CategoryDtoRes save(CategoryDtoReq categoryDtoReq) throws DuplicatedException, InvalidArgumentException, MissingArgumentException, NotFoundException {
        return saveOrUpdateCategory(categoryDtoReq, false);
    }

    /**
     * Saves a List of categories at once.
     * @param arrayCategories An array of objects (categories).
     * @return a massage saying categories where saved.
     */
    public String saveAll(List<CategoryDtoReq> arrayCategories) {
        for (CategoryDtoReq categoryDtoReq : arrayCategories) {
            Category category = mapper.convertValue(categoryDtoReq, Category.class);
            categoryRepository.save(category);
        }
        return "Successfully saved all categories";
    }

    /**
     * Updates an existing category.
     * @param categoryDtoReq The data for the category.
     * @return The updated CategoryDtoRes object.
     * @throws InvalidArgumentException If the request data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     * @throws NotFoundException If a referenced entity is not found.
     */
    public CategoryDtoRes update(CategoryDtoReq categoryDtoReq) throws InvalidArgumentException, MissingArgumentException, NotFoundException, DuplicatedException {
        return saveOrUpdateCategory(categoryDtoReq, true);
    }

    /**
     * Deletes a category by its ID.
     * @param id The ID of the category to delete.
     * @return The CategoryDtoRes object representing the deleted category.
     * @throws NotFoundException    If there is no category with the specified ID.
     */
    public CategoryDtoRes delete(Long id) throws NotFoundException {
        if (categoryRepository.findById(id).isEmpty()) {
            throw new NotFoundException("Category not found");
        } else {
            CategoryDtoRes deletedCategory = mapper.convertValue(categoryRepository.findById(id).get(), CategoryDtoRes.class);
            categoryRepository.deleteById(id);
            return deletedCategory;
        }
    }

    // Utils

    /**
     * Object mapper configuration not to fail on empty beans.
     */
    private void configureObjectMapper() {
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }
    /**
     * Converts a list of Category entities to a list of CategoryDtoRes objects.
     * @param categories The list of Category entities to convert.
     * @return A list of CategoryDtoRes objects.
     */
    private List<CategoryDtoRes> mapToCategoryDto(List<Category> categories) {
        return categories.stream()
                .map(category -> mapper.convertValue(category, CategoryDtoRes.class))
                .collect(Collectors.toList());
    }
    /**
     * Converts a Category entity to a CategoryDtoRes object.
     * @param category The Category entity to convert.
     * @return A CategoryDtoRes object representing the converted category.
     */
    private CategoryDtoRes convertToCategoryDto(Category category) {
        return mapper.convertValue(category, CategoryDtoRes.class);
    }
    /**
     * Checks if a category with the given title already exists.
     * @param title The title to check for duplication.
     * @throws DuplicatedException If there is already a category with the same title.
     */
    private void checkDuplicatedTitle(String title) throws DuplicatedException {
        if (categoryRepository.findByTitle(title).isPresent()) {
            throw new DuplicatedException("There is already a category with that exact name");
        }
    }
    /**
     * Checks if a category with the given ID exists.
     * @param categoryId The ID to check.
     * @throws MissingArgumentException If the category with the specified ID is not found.
     */
    private void checkCategoryExists(Long categoryId) throws MissingArgumentException {
        if (categoryRepository.findById(categoryId).isEmpty()) {
            throw new MissingArgumentException("Could not find category id:" + categoryId);
        }
    }
    /**
     * This method contains common instructions for both save() and update() methods to improve code readability. It also uses a boolean operator to choose between save() or update() to handle id operations.
     * @param categoryDtoReq The data for the category.
     * @param isUpdate A flag indicating whether it is an update operation.
     * @return The saved or updated CategoryDtoRes object.
     * @throws DuplicatedException If there is already a category with the same title.
     * @throws MissingArgumentException If required arguments are missing.
     */
    private CategoryDtoRes saveOrUpdateCategory(CategoryDtoReq categoryDtoReq, boolean isUpdate) throws DuplicatedException, MissingArgumentException {

        checkDuplicatedTitle(categoryDtoReq.getTitle());

        Category category = new Category();
        if (isUpdate) {
            Long id = categoryDtoReq.getId();
            checkCategoryExists(id);
            category.setId(id);
        }

        category.setTitle(categoryDtoReq.getTitle());
        category.setDescription(categoryDtoReq.getDescription());
        category.setImage(categoryDtoReq.getImage());

        return convertToCategoryDto(categoryRepository.save(category));
    }
}