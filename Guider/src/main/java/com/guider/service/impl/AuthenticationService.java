
package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.guider.dtos.request.SignUpRequest;
import com.guider.dtos.request.LoginRequest;
import com.guider.dtos.response.JwtAuthenticationResponse;
import com.guider.dtos.response.UserDtoRes;
import com.guider.exceptions.DuplicatedException;
import com.guider.exceptions.NotFoundException;
import com.guider.exceptions.UserNotExistsException;
import com.guider.persistence.entity.User;
import com.guider.persistence.repository.UserRepository;
import com.guider.service.IAuthenticationService;
import jakarta.mail.MessagingException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AuthenticationService implements IAuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final EmailSenderService emailSenderService;
    private final ObjectMapper mapper;

    public AuthenticationService(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtService jwtService, AuthenticationManager authenticationManager, EmailSenderService emailSenderService, ObjectMapper mapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
        this.emailSenderService = emailSenderService;
        this.mapper = mapper;
    }

    @Override
    public JwtAuthenticationResponse signup(SignUpRequest request) throws MessagingException, DuplicatedException {

        // Check user is not in our database
        checkNewUser(request);

        // Gather info, save new user, generate token and send welcoming email
        var user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(request.getRole())
                .favourites(new ArrayList<Long>())
                .build();
        userRepository.save(user);
        emailSenderService.sendWelcomeEmail(user);
        String jwt = jwtService.generateToken(user.getEmail());

        // return token and user info
        return JwtAuthenticationResponse.builder().token(jwt).userDtoRes(mapper.convertValue(user, UserDtoRes.class)).build();
    }

    @Override
    public JwtAuthenticationResponse login(LoginRequest request) throws UserNotExistsException {

        // Generate token using authenticated user
        checkUserExists(request);

        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new UsernameNotFoundException("We could not find email in our database"));

        // Authenticate user
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));

        var jwt = jwtService.generateToken(user.getEmail());
        return JwtAuthenticationResponse.builder().token(jwt).userDtoRes(mapper.convertValue(user, UserDtoRes.class)).build();

    }

    // Utils

    private void checkNewUser(SignUpRequest request) throws DuplicatedException {
        if (userRepository.findByEmail(request.getEmail()).isPresent()) {
            throw new DuplicatedException("User already exists");
        }
    }

    private void checkUserExists(LoginRequest request) throws UserNotExistsException {
        if (userRepository.findByEmail(request.getEmail()).isEmpty()) {
            throw new UserNotExistsException("User not found");
        }
    }
}
