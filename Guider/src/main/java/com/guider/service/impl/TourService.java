package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.guider.dtos.request.TourDtoReq;
import com.guider.dtos.response.CategoryDtoRes;
import com.guider.dtos.response.LocationDtoRes;
import com.guider.dtos.response.TourDtoRes;
import com.guider.dtos.response.UserDtoRes;
import com.guider.exceptions.*;
import com.guider.persistence.entity.*;
import com.guider.persistence.repository.ITourRepository;
import com.guider.service.IService;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TourService implements IService<TourDtoReq, TourDtoRes> {

    // Attributes
    private final ITourRepository tourRepository;
    private final CategoryService categoryService;
    private final UserService userService;
    private final LocationService locationService;
    private final ObjectMapper mapper;

    public TourService(ITourRepository tourRepository, CategoryService categoryService, UserService userService, LocationService locationService, RatingService ratingService, ObjectMapper mapper) {
        this.tourRepository = tourRepository;
        this.categoryService = categoryService;
        this.userService = userService;
        this.locationService = locationService;
        this.mapper = mapper;
    }

    // Find all
    public List<TourDtoRes> findAll() throws NoContentException {
        if (tourRepository.findAll().isEmpty() || tourRepository.findAll().size() == 0) {
            throw new NoContentException("No tours found");
        } else {
            return tourRepository.findAll()
                    .stream()
                    .map(tour -> mapper.convertValue(tour, TourDtoRes.class))
                    .collect(Collectors.toList());
        }
    }

    // Find by id
    public TourDtoRes findById(Long id) throws InvalidArgumentException, NotFoundException, MissingArgumentException {

        if (id == null) {
            throw new MissingArgumentException("Please, choose a tour");
        } else if (id < 1) {
            throw new InvalidArgumentException("Id must be greater than zero");
        } else if (tourRepository.findById(id).isEmpty()) {
            throw new NotFoundException("Tour id not found");
        } else {
            return mapper.convertValue(tourRepository.findById(id).get(), TourDtoRes.class);
        }
    }

    // Find by guide
    public List<TourDtoRes> findByGuideId(Long id) throws NoContentException {
        if (tourRepository.findAll().isEmpty() || tourRepository.findAll().size() == 0) {
            throw new NoContentException("No tours found for this guide");
        } else {
            return tourRepository.findByGuideId(id)
                    .stream()
                    .map(tour -> mapper.convertValue(tour, TourDtoRes.class))
                    .collect(Collectors.toList());
        }
    }

    // Find by category
    public List<TourDtoRes> findByCategoryId(Long id) throws NoContentException {
        if (tourRepository.findByCategoryId(id).isEmpty() || tourRepository.findByCategoryId(id).size() == 0) {
            throw new NoContentException("No tours found for this guide");
        } else {
            return tourRepository.findByCategoryId(id)
                    .stream()
                    .map(tour -> mapper.convertValue(tour, TourDtoRes.class))
                    .collect(Collectors.toList());
        }
    }

    // Get random tours
    public List<TourDtoRes> getRandomTours() throws NoContentException {
        List<Tour> list = tourRepository.getRandomTours();
        return list
               .stream()
               .map(tour -> mapper.convertValue(tour, TourDtoRes.class))
               .collect(Collectors.toList());
    }

    // Search by location
    public List<TourDtoRes> findByLocation(String locationName) throws NotFoundException {
        List<Tour> list = tourRepository.findByLocationName(locationName);
        if (list.isEmpty()) {
            throw new NotFoundException("Location not found");
        }
        return mapToTourDto(list);
    }

    // Search by location nearby
    public List<TourDtoRes> searchByLocationCoordinatesNear(double latitude, double longitude, double distance) throws NotFoundException {
        List<Tour> list = tourRepository.findByLocationCoordinatesNear(latitude, longitude, distance);
        if (list.isEmpty()) {
            throw new NotFoundException("No available tours nearby");
        }
        return mapToTourDto(list);
    }

    // Save
    public TourDtoRes save(TourDtoReq tourDtoReq) throws DuplicatedException, InvalidArgumentException, MissingArgumentException, NotFoundException {

        validateRequest(tourDtoReq);

        UserDtoRes guide =  userService.findById(tourDtoReq.getGuideId());
        CategoryDtoRes category = categoryService.findById(tourDtoReq.getCategoryId());
        LocationDtoRes location = locationService.findById(tourDtoReq.getLocationId());

        if (tourRepository.findByName(tourDtoReq.getName()).isPresent()) {
            throw new DuplicatedException("There is already a tour with that exact name");
        } else {
            Tour tour = new Tour();
            tour.setGuide(mapper.convertValue(guide, User.class));
            tour.setCategory(mapper.convertValue(category, Category.class));
            tour.setName(tourDtoReq.getName());
            tour.setImages(tourDtoReq.getImages());
            tour.setDurationInHours(tourDtoReq.getDurationInHours());
            tour.setLocation(mapper.convertValue(location, Location.class));
            tour.setPrice(tourDtoReq.getPrice());
            tour.setDescription(tourDtoReq.getDescription());
            tour.setTags(new ArrayList<>());
            tour.setRating(0.00);

            Tour savedTour = tourRepository.save(tour);
            return mapper.convertValue(savedTour, TourDtoRes.class);
        }
    }

    // Save all
    public String saveAll(List<TourDtoReq> arrayTours) throws NotFoundException, InvalidArgumentException, MissingArgumentException {


        for (TourDtoReq tourDtoReq: arrayTours) {
            UserDtoRes guide =  userService.findById(tourDtoReq.getGuideId());
            CategoryDtoRes category = categoryService.findById(tourDtoReq.getCategoryId());
            LocationDtoRes location = locationService.findById(tourDtoReq.getLocationId());

            Tour tour = new Tour();
            tour.setGuide(mapper.convertValue(guide, User.class));
            tour.setCategory(mapper.convertValue(category, Category.class));
            tour.setName(tourDtoReq.getName());
            tour.setImages(tourDtoReq.getImages());
            tour.setDurationInHours(tourDtoReq.getDurationInHours());
            tour.setLocation(mapper.convertValue(location, Location.class));
            tour.setPrice(tourDtoReq.getPrice());
            tour.setDescription(tourDtoReq.getDescription());
            tour.setTags(new ArrayList<>());
            tour.setRating(0.00);

            tourRepository.save(tour);
        }
        return "Succesfully saved all tours";
    }

    // Update
    public TourDtoRes update(TourDtoReq tourDtoReq) throws InvalidArgumentException, MissingArgumentException, NotFoundException {

        validateRequest(tourDtoReq);

        Long id_Tour = tourDtoReq.getId();
        CategoryDtoRes category = categoryService.findById(tourDtoReq.getCategoryId());
        UserDtoRes guide = userService.findById(tourDtoReq.getGuideId());
        LocationDtoRes location = locationService.findById(tourDtoReq.getLocationId());

        if (tourRepository.findById(id_Tour).isEmpty()) {
            throw new MissingArgumentException("Could not find tour id:" + id_Tour);
        } else {
            Tour tourModificado = tourRepository.findById(id_Tour).get();
            tourModificado.setCategory(mapper.convertValue(category, Category.class));
            tourModificado.setDescription(tourDtoReq.getDescription());
            tourModificado.setDurationInHours(tourDtoReq.getDurationInHours());
            tourModificado.setImages(tourDtoReq.getImages());
            tourModificado.setGuide(mapper.convertValue(guide, User.class));
            tourModificado.setName(tourDtoReq.getName());
            tourModificado.setLocation(mapper.convertValue(location, Location.class));
            tourModificado.setPrice(tourDtoReq.getPrice());
            tourModificado.setTags(tourDtoReq.getTags());

            Tour tourActualizado = tourRepository.save(tourModificado);
            return mapper.convertValue(tourActualizado, TourDtoRes.class);
        }
    }

    // Delete
    public TourDtoRes delete(Long id) throws NotFoundException {

        if (tourRepository.findById(id).isEmpty()) {
            throw new NotFoundException("Tour not found");
        } else {
            TourDtoRes deletedTour = mapper.convertValue(tourRepository.findById(id).get(), TourDtoRes.class);
            tourRepository.deleteById(id);
            return deletedTour;
        }
    }

    // Utils

    private void validateRequest(TourDtoReq t) throws MissingArgumentException, InvalidArgumentException {

        if (t.getName() == null || t.getName().isBlank() || t.getName().isEmpty()) {
            throw new MissingArgumentException("Please, write a name for the tour");
        } else if (t.getLocationId() == null) {
            throw new MissingArgumentException("Please, specify a location for the tour");
        } else if (t.getDurationInHours() == null) {
            throw new MissingArgumentException("Please, specify the duration for the tour");
        } else if (t.getDescription() == null || t.getDescription().isBlank() || t.getDescription().isEmpty()) {
            throw new MissingArgumentException("Please, write a description for the tour");
        } else if (t.getDescription().length() > 5000) {
            throw new InvalidArgumentException("Keep your description short: 5000 characters or less");
        } else if (t.getPrice() == null || t.getPrice() < 1) {
            throw new InvalidArgumentException("Please, specify a valid price");
        } //else if (t.getRating() == null || t.getRating() < 1) {
        //throw new InvalidArgumentException("Please, specify a valid rating: from 1 to 5");
        //}
    }

    private List<TourDtoRes> mapToTourDto(List<Tour> tours) {
        return tours.stream()
                .map(tour -> mapper.convertValue(tour, TourDtoRes.class))
                .collect(Collectors.toList());
    }

}
