package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.guider.dtos.request.TagDtoReq;
import com.guider.dtos.response.TagDtoRes;
import com.guider.dtos.response.TourDtoRes;
import com.guider.exceptions.*;
import com.guider.persistence.entity.Tag;
import com.guider.persistence.entity.Tour;
import com.guider.persistence.repository.ITagRepository;
import com.guider.persistence.repository.ITourRepository;
import com.guider.service.IService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing tags and related operations.
 */

@Service
public class TagService implements IService<TagDtoReq, TagDtoRes> {

    // Attributes
    private final ITagRepository tagRepository;
    private final TourService tourService;
    private final ITourRepository tourRepository;
    private final ObjectMapper mapper;

    /**
     * Constructs an instance of {@code TagService} with the provided repositories and mapper.
     * @param tagRepository   The repository for tags.
     * @param tourService     The service for managing tours.
     * @param tourRepository  The repository for tours.
     * @param mapper          The object mapper for converting between DTOs and entities.
     */
    public TagService(ITagRepository tagRepository, TourService tourService, ITourRepository tourRepository, ObjectMapper mapper) {
        this.tagRepository = tagRepository;
        this.tourService = tourService;
        this.tourRepository = tourRepository;
        this.mapper = mapper;
    }

    /**
     * Retrieves all tags and converts them to DTOs.
     * @return A list of {@code TagDtoRes} representing all tags.
     * @throws NoContentException Thrown if there are no tags available.
     */
    public List<TagDtoRes> findAll() throws NoContentException {
        List<Tag> tags = tagRepository.findAll();

        if (tags.isEmpty()) {
            throw new NoContentException("There are no tags");
        }

        return mapToTagDtos(tags);
    }

    /**
     * Retrieves a tag by its ID and converts it to a DTO.
     * @param id The ID of the tag to retrieve.
     * @return The {@code TagDtoRes} representing the tag.
     * @throws MissingArgumentException Thrown if the ID is missing or null.
     * @throws InvalidArgumentException  Thrown if the ID is invalid.
     * @throws NotFoundException        Thrown if no tag is found with the specified ID.
     */
    public TagDtoRes findById(Long id) throws MissingArgumentException, InvalidArgumentException, NotFoundException {
        return tagRepository.findById(id)
                .map(this::convertToTagDto)
                .orElseThrow(()-> new NotFoundException("There is no tag with id " + id));
    }

    /**
     * Saves a new tag based on the provided DTO.
     * @param tagDtoReq The DTO containing tag information.
     * @return The {@code TagDtoRes} representing the saved tag.
     * @throws NotFoundException Thrown if an issue occurs during save.
     */
    public TagDtoRes save(TagDtoReq tagDtoReq) throws NotFoundException {
        return saveOrUpdateTag(tagDtoReq, false);
    }

    /**
     * Updates an existing tag based on the provided DTO.
     * @param tagDtoReq The DTO containing updated tag information.
     * @return The {@code TagDtoRes} representing the updated tag.
     * @throws NotFoundException Thrown if the tag to update is not found.
     */
    public TagDtoRes update(TagDtoReq tagDtoReq) throws NotFoundException {
        return saveOrUpdateTag(tagDtoReq, true);
    }

    /**
     * Deletes a tag by its ID.
     * @param id The ID of the tag to delete.
     * @return The {@code TagDtoRes} representing the deleted tag.
     * @throws NotFoundException Thrown if no tag is found with the specified ID.
     */
    public TagDtoRes delete(Long id) throws NotFoundException {
        Tag tag = tagRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("We could not find tag with id " + id));

        TagDtoRes deleted = convertToTagDto(tag);
        tagRepository.deleteById(id);

        return deleted;
    }

    /**
     * Retrieves tags associated with a tour by its ID.
     * @param tourId The ID of the tour.
     * @return A list of {@code TagDtoRes} representing tags associated with the tour.
     * @throws NoContentException      Thrown if there are no tags associated with the tour.
     * @throws InvalidArgumentException Thrown if the tour ID is invalid.
     * @throws MissingArgumentException Thrown if the tour ID is missing or null.
     * @throws NotFoundException       Thrown if the tour is not found.
     */
    public List<TagDtoRes> findByTourId(Long tourId) throws NoContentException, InvalidArgumentException, MissingArgumentException, NotFoundException {
        List<Long> tagIds = tourService.findById(tourId).getTags();

        if (tagIds.isEmpty()) {
            throw new NoContentException("No tags for this tour");
        }

        return getTagDtos(tagIds);
    }

    /**
     * Adds a tag to a tour.
     * @param tourId The ID of the tour.
     * @param tagId  The ID of the tag to add.
     * @return A list of {@code TagDtoRes} representing tags associated with the tour after the addition.
     * @throws InvalidArgumentException Thrown if the tour ID or tag ID is invalid.
     * @throws MissingArgumentException Thrown if the tour ID or tag ID is missing or null.
     * @throws NotFoundException       Thrown if the tour is not found.
     */
    public List<TagDtoRes> addTagToTour(Long tourId, Long tagId) throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        TourDtoRes tour = tourService.findById(tourId);

        if (tour.getTags().contains(tagId)) {
            return getTagDtos(tour.getTags());
        }

        tour.getTags().add(tagId);
        tourRepository.save(mapper.convertValue(tour, Tour.class));

        return getTagDtos(tour.getTags());
    }

    /**
     * Removes a tag from a tour.
     * @param tourId The ID of the tour.
     * @param tagId  The ID of the tag to remove.
     * @return A list of {@code TagDtoRes} representing tags associated with the tour after the removal.
     * @throws InvalidArgumentException Thrown if the tour ID or tag ID is invalid.
     * @throws MissingArgumentException Thrown if the tour ID or tag ID is missing or null.
     * @throws NotFoundException       Thrown if the tag is not found in the tour.
     */
    public List<TagDtoRes> removeTagFromTour(Long tourId, Long tagId) throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        TourDtoRes tour = tourService.findById(tourId);

        if (!tour.getTags().contains(tourId)) {
            throw new NotFoundException("Could not remove because tag was not found");
        }

        tour.getTags().remove(tagId);
        tourRepository.save(mapper.convertValue(tour, Tour.class));

        return getTagDtos(tour.getTags());
    }


    // Utils

    /**
     * Converts a list of Tag entities to a list of TagDtoRes DTOs.
     * @param tags The list of Tag entities to be converted.
     * @return A list of TagDtoRes DTOs.
     */
    private List<TagDtoRes> mapToTagDtos(List<Tag> tags) {
        return tags.stream()
                .map(tag -> mapper.convertValue(tag, TagDtoRes.class))
                .collect(Collectors.toList());
    }

    /**
     * Converts a Tag entity to a TagDtoRes DTO.
     * @param tag The Tag entity to be converted.
     * @return The TagDtoRes DTO.
     */
    private TagDtoRes convertToTagDto(Tag tag) {
        return mapper.convertValue(tag, TagDtoRes.class);
    }

    /**
     * This method contains common instructions for both save() and update() methods to improve code readability. It also uses a boolean operator to choose between save() or update() to handle id operations.
     * @param tagDtoReq The TagDtoReq DTO containing tag information.
     * @param isUpdate  A boolean flag indicating whether it's an update operation.
     * @return The TagDtoRes representing the saved or updated tag.
     * @throws NotFoundException Thrown if an issue occurs during save or update.
     */
    private TagDtoRes saveOrUpdateTag(TagDtoReq tagDtoReq, boolean isUpdate) throws NotFoundException {
        Tag tag = new Tag();

        if (isUpdate) {
            Long id = tagDtoReq.getId();
            checkTagExists(id);
            tag.setId(id);
        }

        tag.setTitle(tagDtoReq.getTitle());
        tag.setDescription(tagDtoReq.getDescription());
        tag.setImage(tagDtoReq.getImage());
        tag.setColor(tagDtoReq.getColor());

        return convertToTagDto(tagRepository.save(tag));
    }

    /**
     * Checks if a tag with the given ID exists.
     * @param tagId The ID of the tag to check.
     * @throws NotFoundException Thrown if the tag with the specified ID is not found.
     */
    private void checkTagExists(Long tagId) throws NotFoundException {
        if (tagRepository.findById(tagId).isEmpty()) {
            throw new NotFoundException("Could not find tag id:" + tagId);
        }
    }

    /**
     * Retrieves a list of TagDtoRes DTOs based on a list of tag IDs.
     * @param tagIds The list of tag IDs.
     * @return A list of TagDtoRes DTOs representing the tags with the given IDs.
     * @throws InvalidArgumentException Thrown if an invalid argument is encountered.
     * @throws MissingArgumentException  Thrown if a required argument is missing.
     * @throws NotFoundException        Thrown if a tag with a specified ID is not found.
     */
    private List<TagDtoRes> getTagDtos(List<Long> tagIds) throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        List<TagDtoRes> list = new ArrayList<>();
        for (Long id : tagIds) {
            list.add(findById(id));
        }
        return list;
    }
}
