package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.guider.dtos.request.AppointmentDtoReq;
import com.guider.dtos.response.AppointmentDtoRes;
import com.guider.dtos.response.RatingDtoRes;
import com.guider.dtos.response.TourDtoRes;
import com.guider.dtos.response.UserDtoRes;
import com.guider.exceptions.*;
import com.guider.persistence.entity.Appointment;
import com.guider.persistence.entity.Rating;
import com.guider.persistence.entity.Tour;
import com.guider.persistence.entity.User;
import com.guider.persistence.repository.IAppointmentRepository;
import com.guider.persistence.repository.IRatingRepository;
import com.guider.service.IService;
import jakarta.mail.MessagingException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing appointments.
 * This service provides CRUD operations for handling appointments, including
 * finding, saving, updating, and deleting appointments. It also includes utility
 * methods for checking availability, date/time validation, and user calendar checks.
 *
 * @author Elias Gazza
 * @version 1.0
 * @since 2023-12-04
 */
@Service
public class AppointmentService implements IService<AppointmentDtoReq, AppointmentDtoRes> {

    // Attributes
    private final ObjectMapper mapper;
    private final IAppointmentRepository appointmentRepository;
    private final UserService userService;
    private final TourService tourService;
    private final EmailSenderService emailSenderService;

    /**
     * Constructor for AppointmentService.
     *
     * @param mapper                The ObjectMapper for object conversion.
     * @param appointmentRepository The repository for managing appointments.
     * @param userService           The service for managing users.
     * @param tourService           The service for managing tours.
     * @param emailSenderService    The service for managing emails.
     */
    public AppointmentService(ObjectMapper mapper, IAppointmentRepository appointmentRepository, IRatingRepository ratingRepository, UserService userService, TourService tourService, EmailSenderService emailSenderService) {
        this.mapper = mapper;
        this.appointmentRepository = appointmentRepository;
        this.userService = userService;
        this.tourService = tourService;
        this.emailSenderService = emailSenderService;
        configureObjectMapper();
    }

    /**
     * Retrieves a list of all appointments.
     * @return A list of AppointmentDtoRes objects.
     * @throws NoContentException If there are no appointments in the database.
     */
    public List<AppointmentDtoRes> findAll() throws NoContentException {
        List<Appointment> allAppointments = appointmentRepository.findAll();
        if (allAppointments.isEmpty()) {
            throw new NoContentException("No appointments in the database");
        } else {
            return mapToAppointmentDto(allAppointments);
        }
    }

    /**
     * Retrieves an appointment by its ID.
     * @param id The ID of the appointment to retrieve.
     * @return The AppointmentDtoRes object.
     * @throws NotFoundException If there is no appointment with the specified ID.
     */
    public AppointmentDtoRes findById(Long id) throws NotFoundException {
        return appointmentRepository.findById(id)
                .map(this::convertToAppointmentDto)
                .orElseThrow(() -> new NotFoundException("There is no appointment with id " + id));
    }

    /**
     * Retrieves a list of appointments associated with a guide.
     * @param id The ID of the guide.
     * @return A list of AppointmentDtoRes objects.
     * @throws NoContentException If there are no appointments for the specified guide.
     */
    public List<AppointmentDtoRes> findByGuideId(Long id) throws NoContentException {
        List<Appointment> appointments = appointmentRepository.findByGuideId(id);
        if (appointments.isEmpty()) {
            throw new NoContentException("No appointments in the database");
        } else {
            return mapToAppointmentDto(appointments);
        }
    }

    /**
     * Retrieves a list of appointments associated with a user.
     * @param id The ID of the user.
     * @return A list of AppointmentDtoRes objects.
     * @throws NoContentException If there are no appointments for the specified user.
     */
    public List<AppointmentDtoRes> findByUserId(Long id) throws NoContentException {
        List<Appointment> appointments = appointmentRepository.findByUserId(id);
        if (appointments.isEmpty()) {
            throw new NoContentException("No appointments for this person");
        } else {
            return mapToAppointmentDto(appointments);
        }
    }

    /**
     * Retrieves a list of appointments within a specified date range.
     * @param start The start date of the range (inclusive).
     * @param end   The end date of the range (inclusive).
     * @return A list of AppointmentDtoRes objects.
     */
    public List<AppointmentDtoRes> findByDates(LocalDate start, LocalDate end) {
        List<LocalDate> dates = start.datesUntil(end.plusDays(1)).toList();
        List<AppointmentDtoRes> allAppointments = new ArrayList<>();
        for (LocalDate date : dates) {
            List<Appointment> appointment = appointmentRepository.findByDate(date);
            List<AppointmentDtoRes> dtoList = mapToAppointmentDto(appointment);
            if (!dtoList.isEmpty()) {
                allAppointments.addAll(dtoList);
            }
        }
        return allAppointments;
    }

    /**
     * Retrieves a list of appointments associated with a tour.
     * @param id The ID of the tour.
     * @return A list of AppointmentDtoRes objects.
     * @throws NoContentException If there are no appointments for the specified tour.
     */
    public List<AppointmentDtoRes> findByTourId(Long id) throws NoContentException {
        List<Appointment> appointments = appointmentRepository.findByTourId(id);
        if (appointments.isEmpty()) {
            throw new NoContentException("No appointments for this tour");
        } else {
            return mapToAppointmentDto(appointments);
        }
    }

    /**
     * Saves a new appointment.
     * @param appointmentDtoReq The appointment data to save.
     * @return The saved AppointmentDtoRes object.
     * @throws DuplicatedException      If there is a duplication issue with the appointment.
     * @throws NotFoundException        If a referenced entity (tour, user) is not found.
     * @throws InvalidArgumentException  If there is an issue with date/time validation.
     * @throws MissingArgumentException If required arguments are missing in the request.
     */
    public AppointmentDtoRes save(AppointmentDtoReq appointmentDtoReq) throws DuplicatedException, NotFoundException, InvalidArgumentException, MissingArgumentException, MessagingException {
        return saveOrUpdateAppointment(appointmentDtoReq, false);
    }

    /**
     * Updates an existing appointment.
     * @param appointmentDtoReq The appointment data to update.
     * @return The updated AppointmentDtoRes object.
     * @throws DuplicatedException      If there is a duplication issue with the appointment.
     * @throws NotFoundException        If the appointment with the specified ID is not found.
     * @throws InvalidArgumentException  If there is an issue with date/time validation.
     * @throws MissingArgumentException If required arguments are missing in the request.
     */
    public AppointmentDtoRes update(AppointmentDtoReq appointmentDtoReq) throws DuplicatedException, NotFoundException, InvalidArgumentException, MissingArgumentException, MessagingException {
        return saveOrUpdateAppointment(appointmentDtoReq, true);
    }

    /**
     * Deletes an appointment by its ID.
     * @param id The ID of the appointment to delete.
     * @return The AppointmentDtoRes object representing the deleted appointment.
     * @throws NotFoundException    If there is no appointment with the specified ID.
     * @throws InvalidArgumentException If there is an issue with date/time validation.
     */
    public AppointmentDtoRes delete(Long id) throws NotFoundException, InvalidArgumentException {
        Appointment appointment = appointmentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("We could not find appointment with id " + id));

        checkDateTime(mapper.convertValue(appointment, AppointmentDtoReq.class)); // can not delete past appointments

        AppointmentDtoRes deleted = convertToAppointmentDto(appointment);
        appointmentRepository.deleteById(id);

        return deleted;
    }

    // Utils

    /**
     * Object mapper configuration not to fail on empty beans.
     */
    private void configureObjectMapper() {
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    /**
     * Converts a list of Appointment entities to a list of AppointmentDtoRes objects.
     * @param appointments The list of Appointment entities to convert.
     * @return A list of AppointmentDtoRes objects.
     */
    private List<AppointmentDtoRes> mapToAppointmentDto(List<Appointment> appointments) {
        return appointments.stream()
                .map(appointment -> mapper.convertValue(appointment, AppointmentDtoRes.class))
                .collect(Collectors.toList());
    }

    /**
     * Converts an Appointment entity to an AppointmentDtoRes object.
     * @param appointment The Appointment entity to convert.
     * @return An AppointmentDtoRes object representing the converted appointment.
     */
    private AppointmentDtoRes convertToAppointmentDto(Appointment appointment) {
        return mapper.convertValue(appointment, AppointmentDtoRes.class);
    }

    /**
     * This method contains common instructions for both save() and update() methods to improve code readability. It also uses a boolean operator to choose between save() or update() to handle id operations.
     * @param appointmentDtoReq The appointment data to save or update.
     * @param isUpdate          A flag indicating whether it is an update operation.
     * @return The saved or updated AppointmentDtoRes object.
     * @throws DuplicatedException      If there is a duplication issue with the appointment.
     * @throws NotFoundException        If a referenced entity (tour, user) is not found.
     * @throws InvalidArgumentException  If there is an issue with date/time validation.
     * @throws MissingArgumentException If required arguments are missing in the request.
     */
    private AppointmentDtoRes saveOrUpdateAppointment(AppointmentDtoReq appointmentDtoReq, boolean isUpdate)
            throws DuplicatedException, NotFoundException, InvalidArgumentException, MissingArgumentException, MessagingException {

        checkAvailability(appointmentDtoReq);
        checkDateTime(appointmentDtoReq);
        checkUserCalendar(appointmentDtoReq);

        TourDtoRes tour = tourService.findById(appointmentDtoReq.getTourId());
        UserDtoRes user = userService.findById(appointmentDtoReq.getUserId());
        UserDtoRes guide = tour.getGuide();
        LocalDate date = appointmentDtoReq.getDate();
        LocalTime time = appointmentDtoReq.getTime();

        Appointment appointment = new Appointment();

        if (isUpdate) {
            Long id = appointmentDtoReq.getId();
            checkAppointmentExists(id);
            appointment.setId(id);
        }

        appointment.setTour(mapper.convertValue(tour, Tour.class));
        appointment.setUser(mapper.convertValue(user, User.class));
        appointment.setGuide(mapper.convertValue(guide, User.class));
        appointment.setDate(date);
        appointment.setTime(time);

        Appointment ap = appointmentRepository.save(appointment);
        if (!isUpdate) {
            emailSenderService.sendAppointmentEmail(ap);
        }

        return convertToAppointmentDto(ap);
    }

    /**
     * Checks if an appointment with the given ID exists.
     * @param appointmentId The ID to check.
     * @throws NotFoundException If the appointment with the specified ID is not found.
     */
    private void checkAppointmentExists(Long appointmentId) throws NotFoundException {
        if (appointmentRepository.findById(appointmentId).isEmpty()) {
            throw new NotFoundException("Could not find appointment id:" + appointmentId);
        }
    }

    /**
     * Checks the availability of a tour for a specified date and time.
     * @param appointment The appointment data containing the tour ID, date, and time.
     * @throws DuplicatedException      If there is already an appointment for that tour in the specified date/time.
     * @throws InvalidArgumentException  If there is an issue with date/time validation, like date/time in the past.
     * @throws MissingArgumentException If required arguments are missing in the request.
     * @throws NotFoundException        If the referenced tour or its details are not found.
     */
    private void checkAvailability(AppointmentDtoReq appointment) throws DuplicatedException, InvalidArgumentException, MissingArgumentException, NotFoundException {

        LocalDate wantedDate = appointment.getDate();
        LocalTime wantedTime = appointment.getTime();
        Integer tourDuration = tourService.findById(appointment.getTourId()).getDurationInHours();

        List<Appointment> appointments = appointmentRepository.findByTourId(appointment.getTourId());

        for (Appointment element : appointments) {
            if (wantedDate.equals(element.getDate())
                    && wantedTime.isBefore(element.getTime().plusHours(tourDuration))
                    && wantedTime.isAfter(element.getTime().minusHours(tourDuration))) {
                throw new DuplicatedException("There tour is not available on the specified date and time");
            }
        }
    }

    /**
     * Checks whether the specified date and time are valid (not in the past).
     * @param appointment The appointment data containing the date and time.
     * @throws InvalidArgumentException If the date/time is in the past.
     */
    private void checkDateTime(AppointmentDtoReq appointment) throws InvalidArgumentException {
        LocalDate wantedDate = appointment.getDate();
        LocalTime wantedTime = appointment.getTime();
        if (wantedDate.isBefore(LocalDate.now()) && wantedTime.isBefore(LocalTime.now())) {
            throw new InvalidArgumentException("Can not use date/time in the past");
        }
    }

    /**
     * Checks the calendar of a user to avoid overlapping reservations.
     * @param appointment The appointment data containing the user ID, date, and time.
     * @throws DuplicatedException If the user already has another reservation for the specified date and time.
     */
    private void checkUserCalendar(AppointmentDtoReq appointment) throws DuplicatedException {
        LocalDate wantedDate = appointment.getDate();
        LocalTime wantedTime = appointment.getTime();
        Long userId = appointment.getUserId();

        // Get all appointments for the user
        List<Appointment> allAppointments = appointmentRepository.findByUserId(userId);

        // Checking dates and times
        for (Appointment item : allAppointments) {

            LocalDate itemDate = item.getDate();
            LocalTime itemTime = item.getTime();
            LocalTime itemDuration = itemTime.plusHours(item.getTour().getDurationInHours());

            if (wantedDate.isEqual(itemDate)
                    && ((wantedTime.equals(itemTime)
                    || wantedTime.isBefore(itemDuration)))) {
                throw new DuplicatedException("User has a reservation for that day and time");
            }
        }
    }

}