package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.guider.dtos.response.UserDtoRes;
import com.guider.exceptions.MissingArgumentException;
import com.guider.exceptions.NotFoundException;
import com.guider.persistence.entity.User;
import com.guider.persistence.repository.UserRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private static final Logger LOG = LogManager.getLogger(TourService.class);

    @Autowired
    UserRepository userRepository;
    @Autowired
    ObjectMapper mapper;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return new ArrayList<>(userRepository.findAll());
    }

    public User updateUser(User user) throws  MissingArgumentException {

        Long id_User = user.getId();

        if (userRepository.findById(id_User).isEmpty()) {
            LOG.error("Failed to update: user id does not exist");
            throw new MissingArgumentException("User id:" + id_User + " not found");
        } else {
            User modUser = userRepository.findById(id_User).get();
            modUser.setRole(user.getRole());
            modUser.setFirstName(user.getFirstName());
            modUser.setLastName(user.getLastName());
            modUser.setEmail(user.getEmail());
            modUser.setFavourites(user.getFavourites());
            LOG.info("User updated successfully");
            return userRepository.save(modUser);
        }
    }

    public User deleteUser(Long id) throws NotFoundException {

        if (userRepository.findById(id).isEmpty()) {
            LOG.error("Failed to delete: user id does not exist");
            throw new NotFoundException("User not found");
        } else {
            User deletedUser = userRepository.findById(id).get();
            userRepository.deleteById(id);
            LOG.info("User deleted successfully");
            return deletedUser;
        }
    }

    public User loadUserByUsername(String username) {
        return userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public UserDtoRes findById(Long id) throws NotFoundException {
        if (userRepository.findById(id).isEmpty()) {
            throw new NotFoundException("User not found");
        } else {
            User founded = userRepository.findById(id).get();
            return mapper.convertValue(founded, UserDtoRes.class);
        }
    }

}
