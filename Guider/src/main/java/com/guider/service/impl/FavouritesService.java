package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.guider.dtos.response.TourDtoRes;
import com.guider.dtos.response.UserDtoRes;
import com.guider.exceptions.InvalidArgumentException;
import com.guider.exceptions.MissingArgumentException;
import com.guider.exceptions.NoContentException;
import com.guider.exceptions.NotFoundException;
import com.guider.persistence.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service class for managing user favourites and associated tours.
 */

@Service
public class FavouritesService {

    // Attributes
    private final UserService userService;
    private final TourService tourService;
    private final ObjectMapper mapper;

    /**
     * Constructor to initialize the FavouritesService with required dependencies.
     * @param userService The UserService responsible for user-related operations.
     * @param tourService The TourService responsible for tour-related operations.
     * @param mapper      The ObjectMapper for JSON serialization and deserialization.
     */
    public FavouritesService(UserService userService, TourService tourService, ObjectMapper mapper) {
        this.userService = userService;
        this.tourService = tourService;
        this.mapper = mapper;
        configureObjectMapper();
    }

    /**
     * Retrieve a list of TourDtoRes representing the user's favourite tours.
     * @param userId The ID of the user whose favourites are to be retrieved.
     * @return A List of TourDtoRes representing the user's favourite tours.
     * @throws NoContentException        If the user has no favourite tours.
     * @throws InvalidArgumentException If the provided user ID is invalid.
     * @throws MissingArgumentException If the user ID is missing.
     */
    public List<TourDtoRes> findByUserId(Long userId) throws NotFoundException, InvalidArgumentException, MissingArgumentException, NoContentException {
        List<Long> tourIds = userService.findById(userId).getFavourites();

        if (tourIds.isEmpty()) {
            throw new NoContentException("User has no favourites");
        }

        return getTourDtos(tourIds);
    }

    /**
     * Add a tour to the user's list of favourite tours.
     * @param userId The ID of the user.
     * @param tourId The ID of the tour to be added to favourites.
     * @return A List of TourDtoRes representing the updated list of favourite tours.
     * @throws NotFoundException        If the provided user or tour ID is not found.
     * @throws MissingArgumentException If the user ID or tour ID is missing.
     * @throws InvalidArgumentException If the provided user is not found or the tour is already a favourite.
     */
    public List<TourDtoRes> addTourToFavourites(Long userId, Long tourId) throws NotFoundException, MissingArgumentException, InvalidArgumentException {
        UserDtoRes user = userService.findById(userId);

        if (user.getFavourites().contains(tourId)) {
            return getTourDtos(user.getFavourites());
        }

        user.getFavourites().add(tourId);
        userService.updateUser(mapper.convertValue(user, User.class));
        return getTourDtos(user.getFavourites());
    }

    /**
     * Remove a tour from the user's list of favourite tours.
     * @param userId The ID of the user.
     * @param tourId The ID of the tour to be removed from favourites.
     * @return A List of TourDtoRes representing the updated list of favourite tours.
     * @throws NotFoundException        If the provided user, tour, or the tour is not a favourite.
     * @throws MissingArgumentException If the user ID or tour ID is missing.
     * @throws InvalidArgumentException If the provided user is not found.
     */
    public List<TourDtoRes> removeTourFromFavourites(Long userId, Long tourId) throws NotFoundException, MissingArgumentException, InvalidArgumentException {
        UserDtoRes user = userService.findById(userId);

        if (!user.getFavourites().contains(tourId)) {
            throw new NotFoundException("Could not remove because it was not a favourite");
        }

        user.getFavourites().remove(tourId);
        userService.updateUser(mapper.convertValue(user, User.class));
        return getTourDtos(user.getFavourites());
    }

    // Utils
    /**
     * Object mapper configuration not to fail on empty beans.
     */
    private void configureObjectMapper() {
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }
    /**
     * Retrieve a list of TourDtoRes based on the provided list of tour IDs.
     * @param tourIds The list of tour IDs.
     * @return A List of TourDtoRes representing the tours with the given IDs.
     * @throws NotFoundException        If any of the provided tour IDs is not found.
     * @throws InvalidArgumentException If any of the provided tour IDs is invalid.
     * @throws MissingArgumentException If any of the provided tour IDs is missing.
     */
    private List<TourDtoRes> getTourDtos(List<Long> tourIds) throws NotFoundException, InvalidArgumentException, MissingArgumentException {
        List<TourDtoRes> tours = new ArrayList<>();
        for (Long id: tourIds) {
            tours.add(tourService.findById(id));
        }
        return tours;
    }

}
