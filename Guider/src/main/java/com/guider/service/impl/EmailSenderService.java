package com.guider.service.impl;


import com.guider.persistence.entity.Appointment;
import com.guider.persistence.entity.User;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

@Service
public class EmailSenderService {

    // Attributes
    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    public EmailSenderService(TemplateEngine templateEngine, JavaMailSender mailSender) {
        this.templateEngine = templateEngine;
        this.mailSender = mailSender;
    }

    public void sendEmail(Map<String, String> emailParams){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("guiderappok@gmail.com");
        message.setTo(emailParams.get("toEmail"));
        message.setSubject(emailParams.get("subject"));
        message.setText(emailParams.get("body"));

        mailSender.send(message);
    }

    public void sendHtmlEmail(Map<String, String> emailParams) throws MessagingException {

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true,"UTF-8");

        Context context = new Context();
        context.setVariable("name", emailParams.get("name"));
        String processedString = templateEngine.process("default", context);

        mimeMessageHelper.setFrom("guiderappok@gmail.com");
        mimeMessageHelper.setTo(emailParams.get("toEmail"));
        mimeMessageHelper.setSubject("Bienvenido a Guider!");
        mimeMessageHelper.setText(processedString, true);

        ClassPathResource resource = new ClassPathResource("/static/images/logo.png");
        mimeMessageHelper.addInline("logo",resource);

        mailSender.send(mimeMessage);
    }

    public void sendWelcomeEmail(User user) throws MessagingException {

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true,"UTF-8");

        mimeMessageHelper.setFrom("guiderappok@gmail.com");
        mimeMessageHelper.setTo(user.getEmail());
        mimeMessageHelper.setSubject("Bienvenido a Guider!");

        Context context = new Context();

        context.setVariable("name", user.getFirstName());

        String processedString = (user.getRole().toString().equals("USER")) ?
                templateEngine.process("welcomeUser", context) :
                templateEngine.process("welcomeGuider", context);

        mimeMessageHelper.setText(processedString, true);

        ClassPathResource resource = new ClassPathResource("/static/images/logo.png");
        mimeMessageHelper.addInline("logo",resource);

        mailSender.send(mimeMessage);
    }

    public void sendAppointmentEmail(Appointment appointment) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true,"UTF-8");

        mimeMessageHelper.setFrom("guiderappok@gmail.com");
        mimeMessageHelper.setTo(appointment.getUser().getEmail());
        mimeMessageHelper.setSubject("Confirmación de reserva");
        Context context = new Context();

        context.setVariable("name", appointment.getTour().getName());
        context.setVariable("userName", appointment.getUser().getFirstName());
        context.setVariable("location", appointment.getTour().getLocation());
        context.setVariable("image", appointment.getTour().getImages().get(1));
        context.setVariable("description", appointment.getTour().getDescription());
        context.setVariable("appointmentDate", appointment.getDate());
        context.setVariable("appointmentTime",appointment.getTime() );
        context.setVariable("guiderFirstName", appointment.getGuide().getFirstName());
        context.setVariable("guiderLastName", appointment.getGuide().getLastName());

        String processedString = templateEngine.process("appointmentConfirmation", context);
        mimeMessageHelper.setText(processedString, true);

        ClassPathResource resource = new ClassPathResource("/static/images/logo.png");
        mimeMessageHelper.addInline("logo",resource);


        mailSender.send(mimeMessage);
    }

}
