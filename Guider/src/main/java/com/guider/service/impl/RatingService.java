package com.guider.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.guider.dtos.request.RatingDtoReq;
import com.guider.dtos.response.RatingDtoRes;
import com.guider.exceptions.*;
import com.guider.persistence.entity.Appointment;
import com.guider.persistence.entity.Rating;
import com.guider.persistence.entity.Tour;
import com.guider.persistence.repository.IAppointmentRepository;
import com.guider.persistence.repository.IRatingRepository;
import com.guider.persistence.repository.ITourRepository;
import com.guider.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RatingService implements IService<RatingDtoReq, RatingDtoRes> {

    // Attributes
    private final ObjectMapper mapper;
    private final IRatingRepository ratingRepository;
    private final IAppointmentRepository appointmentRepository;
    private final ITourRepository tourRepository;


    public RatingService(ObjectMapper mapper, IRatingRepository ratingRepository, IAppointmentRepository appointmentRepository, ITourRepository tourRepository) {
        this.mapper = mapper;
        this.ratingRepository = ratingRepository;
        this.appointmentRepository = appointmentRepository;
        this.tourRepository = tourRepository;
        configureObjectMapper();
    }

    @Override
    public List<RatingDtoRes> findAll() throws Exception {
        List<Rating> allRatings = ratingRepository.findAll();
        if (allRatings.isEmpty()) {
            throw new NoContentException("No ratings in the database");
        } else {
            return mapToRatingsDto(allRatings);
        }
    }

    @Override
    public RatingDtoRes findById(Long id) throws InvalidArgumentException, NotFoundException, MissingArgumentException {
        return ratingRepository.findById(id)
                .map(this::convertToRatingDto)
                .orElseThrow(() -> new NotFoundException("There is no rating with id " + id));
    }

    @Override
    public RatingDtoRes save(RatingDtoReq ratingDtoReq) throws NotFoundException, DuplicatedException {
        return saveorUpdate(ratingDtoReq, false);
    }

    @Override
    public RatingDtoRes update(RatingDtoReq ratingDtoReq) throws InvalidArgumentException, NotFoundException, DuplicatedException {
        return saveorUpdate(ratingDtoReq, true);
    }

    @Override
    public RatingDtoRes delete(Long id) throws NotFoundException, InvalidArgumentException {
        Rating rating = ratingRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("We could not find rating with id " + id));

        RatingDtoRes deleted = convertToRatingDto(rating);
        ratingRepository.deleteById(id);

        return deleted;
    }

    public List<RatingDtoRes> findByTourId(Long id) throws NoContentException {
        List<Rating> ratings = ratingRepository.findByTourId(id);
        if (ratings.isEmpty()) {
            throw new NoContentException("No appointments in the database");
        } else {
            return mapToRatingsDto(ratings);
        }
    }

    // Utils

    private RatingDtoRes convertToRatingDto(Rating rating) {
        return mapper.convertValue(rating, RatingDtoRes.class);
    }

    private List<RatingDtoRes> mapToRatingsDto(List<Rating> ratings) {
        return ratings.stream()
                .map(rating -> mapper.convertValue(rating, RatingDtoRes.class))
                .collect(Collectors.toList());
    }

    private void checkRatingExists(Long ratingId) throws NotFoundException {
        if (ratingRepository.findById(ratingId).isEmpty()) {
            throw new NotFoundException("There is no rating with id " + ratingId);
        }
    }

    private RatingDtoRes saveorUpdate(RatingDtoReq ratingDtoReq, boolean isUpdate) throws NotFoundException, DuplicatedException {

        Long appointmentId = ratingDtoReq.getAppointmentId();

        if (!isUpdate) {
            checkAppointmentHasRating(appointmentId);
        }

        Appointment appointment = appointmentRepository.findById(appointmentId)
                .orElseThrow(()-> new NotFoundException("There is no appointment with id " + appointmentId));


        Rating rating = new Rating();

        if (isUpdate) {
            Long id = ratingDtoReq.getId();
            checkRatingExists(id);
            rating.setId(id);
        }

        rating.setAppointment(appointment);
        rating.setTourId(appointment.getTour().getId());
        rating.setUserName(appointment.getUser().getFirstName());
        rating.setStars(ratingDtoReq.getStars());
        rating.setReview(ratingDtoReq.getReview());
        ratingRepository.save(rating);

        calculateTourAverageRating(rating.getTourId());
        return convertToRatingDto(rating);
    }

    private void configureObjectMapper() {
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    private void checkAppointmentHasRating(Long appointmentId) throws DuplicatedException {
        if (ratingRepository.findByAppointmentId(appointmentId).isPresent()) {
            throw new DuplicatedException("This appointment has already been rated");
        }
    }

    private void calculateTourAverageRating(Long tourId) throws NotFoundException {

        double avarageRating = ratingRepository.findByTourId(tourId)
                .stream()
                .mapToDouble(Rating::getStars)
                .average()
                .orElse(0.0);

        if (tourRepository.findById(tourId).isEmpty()) {
            throw new NotFoundException("Could not find tour id: " + tourId);
        } else {
            Tour tour = tourRepository.findById(tourId).get();
            tour.setRating(avarageRating);
            tourRepository.save(tour);
        }
    }
}
