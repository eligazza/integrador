package com.guider.service;

import com.guider.exceptions.*;
import jakarta.mail.MessagingException;

import java.util.List;

public interface IService<Q, S> {

    List<S> findAll() throws Exception;

    S findById(Long id) throws InvalidArgumentException, NotFoundException, MissingArgumentException;

    S save(Q q) throws DuplicatedException, InvalidArgumentException, MissingArgumentException, NotFoundException, MessagingException;

    S update(Q q) throws InvalidArgumentException, MissingArgumentException, NotFoundException, DuplicatedException, MessagingException;

    S delete(Long id) throws NotFoundException, InvalidArgumentException;

}
