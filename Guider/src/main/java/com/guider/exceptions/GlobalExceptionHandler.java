package com.guider.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOG = LogManager.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({NoContentException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleNoContentException(NoContentException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                1001,
                "No content",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                1002,
                "Not found",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler({DuplicatedException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ErrorResponse> handleDuplicatedException(DuplicatedException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                1003,
                "Resource already exists",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorResponse);
    }

    @ExceptionHandler({MissingArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleMissingArgumentException(MissingArgumentException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                2001,
                "Missing argument",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler({InvalidArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleInvalidArgumentException(InvalidArgumentException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                2002,
                "Invalid argument",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler({DuplicatedUserException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ErrorResponse> handleDuplicatedUserException(DuplicatedUserException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                3001,
                "Duplicated user",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorResponse);
    }

    @ExceptionHandler({UserNotExistsException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleUserNotExistsException(UserNotExistsException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                3002,
                "User not exists",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler({InvalidTokenException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ErrorResponse> handleInvalidTokenException(InvalidTokenException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                3003,
                "Invalid token",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorResponse);
    }

    @ExceptionHandler({AuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ErrorResponse> handleAuthenticationException(AuthenticationException e) {
        e.printStackTrace();
        LOG.error(e.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                3004,
                "Authentication error",
                e.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorResponse);
    }
}
