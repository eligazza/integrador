package com.guider.controller;
import com.guider.dtos.request.TourDtoReq;
import com.guider.dtos.response.TourDtoRes;
import com.guider.exceptions.*;
import com.guider.service.impl.TourService;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class for managing tours.
 * Handles operations related to retrieving, creating, updating, and deleting tours.
 */

@RestController
@RequestMapping("/tours")
@Tag(name = "Tours")
public class TourController {

    // Attributes
    private final TourService tourService;

    /**
     * Constructs an instance of {@code TourController} with the provided {@code TourService}.
     * @param tourService The service responsible for managing tours.
     */

    public TourController(TourService tourService) {
        this.tourService = tourService;
    }

    /**
     * Retrieves all stored tours.
     * @return A list of all tours.
     * @throws NoContentException If no tours are found.
     */

    @GetMapping()
    @Operation(summary = "Get all stored tours.")
    @ApiResponse(responseCode = "200", description = "Display all stored tours.")
    public ResponseEntity<List<TourDtoRes>> findAll() throws NoContentException {
        List<TourDtoRes> list = tourService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Creates a new tour (Needs authentication).
     * @param tour The tour data to be saved.
     * @return The newly created tour.
     * @throws DuplicatedException If a tour with the same data already exists.
     * @throws InvalidArgumentException If the provided tour data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     * @throws NotFoundException If referenced entities are not found.
     */

    @PostMapping()
    @Operation(summary = "Create a new tour.")
    @ApiResponse(responseCode = "201", description = "Tour has been created.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<TourDtoRes> saveTour(@RequestBody TourDtoReq tour) throws DuplicatedException, InvalidArgumentException, MissingArgumentException, NotFoundException {
        TourDtoRes newTour = tourService.save(tour);
        return ResponseEntity.status(HttpStatus.CREATED).body(newTour);
    }

    /**
     * Modifies an existing tour (Needs authentication).
     * @param tourDtoReq The tour data to be updated (including the ID).
     * @return The updated tour.
     * @throws InvalidArgumentException If the provided tour data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     * @throws NotFoundException If referenced entities are not found.
     */

    @PutMapping()
    @Operation(summary = "Modify an existing tour.")
    @ApiResponse(responseCode = "200", description = "Tour has been modified.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<TourDtoRes> changeTour(@RequestBody TourDtoReq tourDtoReq) throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(tourService.update(tourDtoReq));
    }

    /**
     * Retrieves a specific tour by ID.
     * @param id The ID of the tour to retrieve.
     * @return The tour with the specified ID.
     * @throws InvalidArgumentException If the ID is invalid.
     * @throws MissingArgumentException If the ID is missing.
     * @throws NotFoundException If the tour is not found.
     */

    @GetMapping("/{id}")
    @Operation(summary = "Find a specific tour.")
    @ApiResponse(responseCode = "200", description = "Display tour.")
    public ResponseEntity<TourDtoRes> findTourById(
            @Parameter(required = true)
            @PathVariable Long id)
            throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        TourDtoRes tourFound = tourService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(tourFound);
    }

    /**
     * Deletes a specific tour (Needs authentication).
     * @param id The ID of the tour to be deleted.
     * @return The deleted tour.
     * @throws NotFoundException If the tour is not found.
     */

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a specific tour.")
    @ApiResponse(responseCode = "200", description = "Tour has been deleted.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<TourDtoRes> deleteTour(@PathVariable Long id) throws NotFoundException {
        TourDtoRes deletedTour = tourService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body(deletedTour);
    }

    /**
     * Retrieves all tours for a specific guide.
     * @param id The ID of the guide.
     * @return A list of tours for the specified guide.
     * @throws NoContentException If no tours are found.
     */

    @GetMapping("/by-guide-id")
    @Operation(summary = "Get all the tours for a specific guide.")
    @ApiResponse(responseCode = "200", description = "Display all tours for the specified guide.")
    public ResponseEntity<List<TourDtoRes>> findToursByGuide(
            @Parameter(required = true)
            @RequestParam("guideId") Long id)
            throws NoContentException {
        List<TourDtoRes> list = tourService.findByGuideId(id);
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Retrieves all tours for a specific category.
     * @param id The ID of the category.
     * @return A list of tours for the specified category.
     * @throws NoContentException If no tours are found.
     */

    @GetMapping("/by-category-id")
    @Operation(summary = "Get all the tours for a specific category.")
    @ApiResponse(responseCode = "200", description = "Display all tours for the specified category.")
    public ResponseEntity<List<TourDtoRes>> findToursByCategory(
            @Parameter(required = true)
            @RequestParam("categoryId") Long id)
            throws NoContentException {
        List<TourDtoRes> list = tourService.findByCategoryId(id);
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Retrieves all tours for a specific location.
     * @param locationName The name of the location.
     * @return ResponseEntity with a list of TourDtoRes objects representing all tours for the specified location.
     * @throws NotFoundException Thrown when no tours are found for the specified location.
     */

    @GetMapping("/by-location")
    @Operation(summary = "Get all the tours for a specific location.")
    @ApiResponse(responseCode = "200", description = "Display all tours for the specified location.")
    public ResponseEntity<List<TourDtoRes>> findByLocation(
            @Parameter(required = true)
            String locationName)
            throws NotFoundException {
        List<TourDtoRes> list = tourService.findByLocation(locationName);
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Retrieves all tours available nearby a specified location.
     * @param latitude  The latitude of the reference location.
     * @param longitude The longitude of the reference location.
     * @param distance  The distance within which to search for nearby tours.
     * @return ResponseEntity with a list of TourDtoRes objects representing all tours available nearby.
     * @throws NotFoundException Thrown when no tours are found nearby the specified location.
     */

    @GetMapping("/nearby")
    @Operation(summary = "Get all the tours available nearby.")
    @ApiResponse(responseCode = "200", description = "Display all tours available nearby.")
    public ResponseEntity<List<TourDtoRes>> searchByLocationCoordinatesNear(
            @Parameter
            double latitude,
            @Parameter
            double longitude,
            @Parameter
            double distance)
            throws NotFoundException {
        List<TourDtoRes> list = tourService.searchByLocationCoordinatesNear(latitude, longitude, distance);
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Retrieves a certain number of randomly-chosen tours.
     * @return A list of randomly-chosen tours.
     * @throws NoContentException If no tours are found.
     */

    @GetMapping("/random")
    @Operation(summary = "Get a given number of tours randomly from the database.")
    @ApiResponse(responseCode = "200", description = "Display a given number of random tours.")
    public ResponseEntity<List<TourDtoRes>> getRandomTours() throws NoContentException {
        List<TourDtoRes> list = tourService.getRandomTours();
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Bulk upload of tours (Needs authentication)
     * IMPORTANT: Make sure to drop any previous table. It uses the id of categories that should be from 1 to 4, as in the categories bulk upload)
     * @param tours The list of tour data to be uploaded.
     * @return A message indicating the success of the upload.
     * @throws InvalidArgumentException If the provided tour data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     * @throws NotFoundException If referenced entities are not found.
     */

    @Hidden
    @PostMapping("/bulk")
    @Operation(summary = "Bulk tours upload")
    @ApiResponse(responseCode = "200", description = "All tours have been uploaded.")
    @ApiResponse(responseCode = "*", description = "Something went wrong.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<String> saveAll(@RequestBody List<TourDtoReq> tours) throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        String newCategory = tourService.saveAll(tours);
        return ResponseEntity.status(HttpStatus.OK).body(newCategory);
    }
}
