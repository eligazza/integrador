package com.guider.controller;

import com.guider.dtos.request.SignUpRequest;
import com.guider.dtos.request.LoginRequest;
import com.guider.dtos.response.JwtAuthenticationResponse;

import com.guider.exceptions.DuplicatedException;
import com.guider.exceptions.NotFoundException;
import com.guider.exceptions.UserNotExistsException;
import com.guider.service.impl.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller class for authentication-related operations.
 * Handles user sign up and sign in.
 */

@RestController
@RequestMapping("/api/v1/auth")
@Tag(name = "Accounts")
public class AuthenticationController {

    // Attributes
    private final AuthenticationService authenticationService;

    /**
     * Constructs an instance of {@code AuthenticationController} with the provided {@code AuthenticationService}.
     * @param authenticationService The service responsible for authenticating users.
     */

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /**
     * Sign up (register) as a new user.
     * @param request The sign-up request data.
     * @return A response entity containing the result of the sign-up operation.
     */

    @PostMapping("/signup")
    @Operation(summary = "Sign up (register) as a new user.")
    @ApiResponse(responseCode = "200", description = "Registration successful.")
    public ResponseEntity<JwtAuthenticationResponse> signup(
            @Parameter(required = true)
            @RequestBody SignUpRequest request) throws MessagingException, DuplicatedException {
        return ResponseEntity.ok(authenticationService.signup(request));
    }

    /**
     * Sign in/Log in as a registered user.
     * @param request The sign-in request data.
     * @return A response entity containing the result of the sign-in operation.
     */

    @PostMapping("/login")
    @Operation(summary = "Log in as a registered user.")
    @ApiResponse(responseCode = "200", description = "Login successful.")
    public ResponseEntity<JwtAuthenticationResponse> login(
            @Parameter(required = true)
            @RequestBody LoginRequest request) throws UserNotExistsException {
        return ResponseEntity.ok(authenticationService.login(request));
    }
}
