package com.guider.controller;

import com.guider.dtos.response.TourDtoRes;
import com.guider.exceptions.InvalidArgumentException;
import com.guider.exceptions.MissingArgumentException;
import com.guider.exceptions.NoContentException;
import com.guider.exceptions.NotFoundException;
import com.guider.service.impl.FavouritesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class for managing user favourite tours.
 * Handles operations related to adding or removing a tour from a list of favourite tours for that user, and also retrieving the list.
 */

@RestController
@RequestMapping("/favs")
@Tag(name = "Favourites")
@SecurityRequirement(name = "bearer-key")
public class FavouriteController {

    // Attributes
    private final FavouritesService favouritesService;

    /**
     * Constructs an instance of {@code FavouriteController} with the provided {@code FavouritesService}.
     * @param favouritesService The service responsible for managing user's favourite tours.
     */

    public FavouriteController(FavouritesService favouritesService) {
        this.favouritesService = favouritesService;
    }

    /**
     * Retrieves all favourite tours for a specific user (Needs authentication).
     * @param userId The ID of the user.
     * @return A list of favourite tours for the specified user.
     * @throws NotFoundException If the user or favourite tours are not found.
     * @throws InvalidArgumentException If the user ID is invalid.
     * @throws MissingArgumentException If the user ID is missing.
     */

    @GetMapping("/{userId}")
    @Operation(summary = "Get all favourite tours for a specific user.")
    @ApiResponse(responseCode = "200", description = "Displaying all favourites for this user.")
    public ResponseEntity<List<TourDtoRes>> findAllFavourites(
            @Parameter(required = true)
            @PathVariable Long userId)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException, NoContentException {
        return ResponseEntity.status(HttpStatus.OK).body(favouritesService.findByUserId(userId));
    }

    /**
     * Adds a tour to a user's favourites list (Needs authentication).
     * @param userId The ID of the user.
     * @param tourId The ID of the tour to be added to favourites.
     * @return A list of favourite tours after the addition.
     * @throws NotFoundException If the user or tour is not found.
     * @throws InvalidArgumentException If the user ID or tour ID is invalid.
     * @throws MissingArgumentException If the user ID or tour ID is missing.
     */

    @PostMapping("/add")
    @Operation(summary = "Add a tour to some user's favourites.")
    @ApiResponse(responseCode = "200", description = "Added to favourites.")
    public ResponseEntity<List<TourDtoRes>> addToFavourites(
            @Parameter(required = true)
            @RequestParam Long userId,
            @Parameter(required = true)
            @RequestParam Long tourId)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException {
        return ResponseEntity.status(HttpStatus.OK).body(favouritesService.addTourToFavourites(userId, tourId));
    }

    /**
     * Removes a tour from a user's favourites list (Needs authentication).
     * @param userId The ID of the user.
     * @param tourId The ID of the tour to be removed from favourites.
     * @return A list of favourite tours after the removal.
     * @throws NotFoundException If the user or tour is not found.
     * @throws InvalidArgumentException If the user ID or tour ID is invalid.
     * @throws MissingArgumentException If the user ID or tour ID is missing.
     */

    @DeleteMapping("/remove")
    @Operation(summary = "Remove a tour from some user's favourites.")
    @ApiResponse(responseCode = "200", description = "Removed from favourites.")
    public ResponseEntity<List<TourDtoRes>> removeFromFavourites(
            @Parameter(required = true)
            @RequestParam Long userId,
            @Parameter(required = true)
            @RequestParam Long tourId)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException {
        return ResponseEntity.status(HttpStatus.OK).body(favouritesService.removeTourFromFavourites(userId, tourId));
    }
}
