package com.guider.controller;

import com.guider.service.impl.EmailSenderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

/**
 * Controller for handling sending email and its related operations.
 * All endpoints are mapped under "/email".
 */

@RestController
@RequestMapping("/email")
@Tag(name = "Emails")
public class EmailController {

    // Attributes
    private final EmailSenderService emailSenderService;

    /**
     * Constructs an instance of {@code EmailController} with the provided {@code EmailSenderService}.
     * @param emailSenderService The service responsible for sending emails.
     */

    public EmailController(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    /**
     * Endpoint for sending plain text email.
     * @param emailParams A Map containing email parameters (e.g., to, subject, body).
     * @return A success message indicating that the email has been sent successfully.
     */

    @PostMapping("/send")
    public String sendEmail(@RequestBody Map<String, String> emailParams) {
        emailSenderService.sendEmail(emailParams);
        return "Correo enviado exitosamente";
    }

    /**
     * Endpoint for sending HTML-formatted email.
     * @param emailParams A Map containing email parameters (e.g., to, subject, body).
     * @return A success message indicating that the HTML-formatted email has been sent successfully.
     * @throws MessagingException Thrown if there is an issue with email message creation or sending.
     */

    @PostMapping("/sendHtml")
    public String sendHtmlEmail(@RequestBody Map<String, String> emailParams) throws MessagingException {
        emailSenderService.sendHtmlEmail(emailParams);
        return "Correo enviado exitosamente";
    }
}