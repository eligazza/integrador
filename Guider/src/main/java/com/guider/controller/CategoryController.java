package com.guider.controller;

import com.guider.dtos.request.CategoryDtoReq;
import com.guider.dtos.response.CategoryDtoRes;
import com.guider.exceptions.*;
import com.guider.service.impl.CategoryService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class for managing categories.
 * Handles operations related to retrieving, creating, updating, and deleting categories.
 */

@RestController
@RequestMapping("/categories")
@Tag(name = "Categories")
public class CategoryController {

    // Attributes
    private final CategoryService categoryService;

    /**
     * Constructs an instance of {@code CategoryController} with the provided {@code CategoryService}.
     * @param categoryService The service responsible for sending emails.
     */

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * Retrieves all stored categories.
     * @return A list of all categories.
     * @throws NoContentException If no categories are found.
     */

    @GetMapping()
    @Operation(summary = "Get all stored categories.")
    @ApiResponse(responseCode = "200", description = "Display all stored categories.")
    public ResponseEntity<List<CategoryDtoRes>> findAll() throws NoContentException {
        List<CategoryDtoRes> list = categoryService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Retrieves a specific category by ID.
     * @param id The ID of the category to retrieve.
     * @return The category with the specified ID.
     * @throws MissingArgumentException If the ID is missing.
     * @throws NotFoundException If the category is not found.
     */

    @GetMapping("/{id}")
    @Operation(summary = "Find a specific category.")
    @ApiResponse(responseCode = "200", description = "Display category.")
    public ResponseEntity<CategoryDtoRes> findCategory(
            @Parameter(required = true)
            @PathVariable Long id)
            throws MissingArgumentException, NotFoundException {
        CategoryDtoRes categoryFound = categoryService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(categoryFound);
    }

    /**
     * Creates a new category (needs authentication).
     * @param category The category data to be saved.
     * @return The newly created category.
     * @throws DuplicatedException If a category with the same data already exists.
     * @throws InvalidArgumentException If the provided category data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     */

    @PostMapping()
    @Operation(summary = "Create a new category.")
    @ApiResponse(responseCode = "201", description = "Category has been created.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<CategoryDtoRes> saveCategory(
            @Parameter(required = true)
            @RequestBody CategoryDtoReq category)
            throws DuplicatedException, InvalidArgumentException, MissingArgumentException, NotFoundException {
        CategoryDtoRes newCategory = categoryService.save(category);
        return ResponseEntity.status(HttpStatus.CREATED).body(newCategory);
    }

    /**
     * Modifies an existing category (Needs authentication).
     * @param categoryDtoReq The category data to be updated.
     * @return The updated category.
     * @throws InvalidArgumentException If the provided category data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     */

    @PutMapping
    @Operation(summary = "Modify an existing category.")
    @ApiResponse(responseCode = "200", description = "Category has been modified.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<CategoryDtoRes> updateCategory(
            @Parameter(required = true)
            @RequestBody CategoryDtoReq categoryDtoReq)
            throws InvalidArgumentException, MissingArgumentException, NotFoundException, DuplicatedException {
        return ResponseEntity.status(HttpStatus.OK).body(categoryService.update(categoryDtoReq));
    }

    /**
     * Deletes a specific category (Needs authentication).
     * @param id The ID of the category to be deleted.
     * @return The deleted category.
     * @throws NotFoundException If the category is not found.
     */

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a specific category.")
    @ApiResponse(responseCode = "200", description = "Category has been deleted.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<CategoryDtoRes> deleteCategory(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException {
        CategoryDtoRes deletedCategory = categoryService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body(deletedCategory);
    }

    /**
     * Bulk upload of categories (Needs authentication)
     * IMPORTANT: Make sure to drop any previous table in order to get the first available ids.
     * @param categories The list of category data to be uploaded.
     * @return A message indicating the success of the upload.
     */

    @Hidden
    @PostMapping("/bulk")
    @Operation(summary = "Bulk categories upload")
    @ApiResponse(responseCode = "200", description = "All categories have been uploaded.")
    @ApiResponse(responseCode = "*", description = "Something went wrong.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<String> saveAll(
            @RequestBody List<CategoryDtoReq> categories) {
        String newCategory = categoryService.saveAll(categories);
        return ResponseEntity.status(HttpStatus.OK).body(newCategory);
    }
}
