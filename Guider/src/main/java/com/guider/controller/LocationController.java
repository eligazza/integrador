package com.guider.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.guider.dtos.response.LocationDtoRes;
import com.guider.exceptions.NoContentException;
import com.guider.exceptions.NotFoundException;
import com.guider.service.impl.LocationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller class for managing locations.
 */

@RestController
@RequestMapping("/locations")
@Tag(name = "Locations")
public class LocationController {

    // Attributes
    private final LocationService locationService;

    /**
     * Constructor for LocationController.
     * @param mapper          The ObjectMapper for converting entities to DTOs.
     * @param locationService The service for managing locations.
     */

    public LocationController(ObjectMapper mapper, LocationService locationService) {
        this.locationService = locationService;
    }

    /**
     * Retrieves all locations.
     * @return ResponseEntity with a list of LocationDtoRes objects representing all locations.
     * @throws NoContentException Thrown when no locations are found in the database.
     */

    @GetMapping()
    @Operation(summary = "Get all locations.")
    @ApiResponse(responseCode = "200", description = "Display all location.")
    public ResponseEntity<List<LocationDtoRes>> findAll() throws NoContentException {
        return ResponseEntity.status(HttpStatus.OK).body(locationService.findAll());
    }

    /**
     * Retrieves a location by its ID.
     * @param id The ID of the location to retrieve.
     * @return ResponseEntity with the LocationDtoRes object representing the found location.
     * @throws NotFoundException Thrown when no location is found with the specified ID.
     */

    @GetMapping("/{id}")
    @Operation(summary = "Find a specific location.")
    @ApiResponse(responseCode = "200", description = "Display location.")
    public ResponseEntity<LocationDtoRes> findById(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException {
        LocationDtoRes locationFound = locationService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(locationFound);
    }

}
