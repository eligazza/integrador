package com.guider.controller;

import com.guider.dtos.request.AppointmentDtoReq;
import com.guider.dtos.request.RatingDtoReq;
import com.guider.dtos.response.AppointmentDtoRes;
import com.guider.dtos.response.RatingDtoRes;
import com.guider.exceptions.*;
import com.guider.service.impl.RatingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ratings")
@Tag(name = "Ratings")
public class RatingController {

    // Attributes
    private final RatingService ratingService;

    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping()
    @Operation(summary = "Get all stored reviews.")
    @ApiResponse(responseCode = "200", description = "Display all stored reviews.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<List<RatingDtoRes>> findAll() throws Exception {
        List<RatingDtoRes> ratings = ratingService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(ratings);
    }

    @PostMapping()
    @Operation(summary = "Rating appointments with 1-5 stars.")
    @ApiResponse(responseCode = "200", description = "Display rated appointment.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<RatingDtoRes> rateAppointment(
            @Parameter(required = true)
            @RequestBody RatingDtoReq ratingDtoReq)
            throws NotFoundException, DuplicatedException {
        RatingDtoRes rating = ratingService.save(ratingDtoReq);
        return ResponseEntity.status(HttpStatus.OK).body(rating);
    }

    @PutMapping
    @Operation(summary = "Modify an existing rating.")
    @ApiResponse(responseCode = "200", description = "Rating has been modified.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<RatingDtoRes> update(
            @Parameter(required = true)
            @RequestBody RatingDtoReq ratingDtoReq)
            throws DuplicatedException, NotFoundException, InvalidArgumentException, MissingArgumentException, MessagingException {
        RatingDtoRes updatedRating = ratingService.update(ratingDtoReq);
        return ResponseEntity.status(HttpStatus.OK).body(updatedRating);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a specific rating.")
    @ApiResponse(responseCode = "200", description = "Rating has been deleted.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<RatingDtoRes> delete(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException {
        RatingDtoRes deletedRating = ratingService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body(deletedRating);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Find a specific rating.")
    @ApiResponse(responseCode = "200", description = "Display appointment.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<RatingDtoRes> findById(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException {
        RatingDtoRes rating = ratingService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(rating);
    }

    @GetMapping("/by-tour-id")
    @Operation(summary = "Get all the ratings for a specific tour.")
    @ApiResponse(responseCode = "200", description = "Display ratings for the specified tour.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<List<RatingDtoRes>> findByTourId(
            @Parameter(name = "tourId", required = true)
            @RequestParam("tourId") Long id)
            throws NoContentException {
        List<RatingDtoRes> ratings = ratingService.findByTourId(id);
        return ResponseEntity.status(HttpStatus.OK).body(ratings);
    }
}
