package com.guider.controller;

import com.guider.dtos.response.UserDtoRes;
import com.guider.exceptions.*;

import com.guider.persistence.entity.User;
import com.guider.service.impl.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class for managing users.
 * Handles operations related to retrieving, updating, and deleting users.
 */
@RestController
@RequestMapping("/users")
@Tag(name = "Users")
@SecurityRequirement(name = "bearer-key")
public class UserController {

    // Attributes
    private final UserService userService;

    /**
     * Constructs an instance of {@code UserController} with the provided {@code UserController}.
     * @param userService The service responsible for managing users.
     */

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Retrieves all stored users.
     *
     * @return A list of all users.
     * @throws NoContentException If no users are found.
     */

    @GetMapping()
    @Operation(summary = "Get all stored users.")
    @ApiResponse(responseCode = "200", description = "Display all stored users.")
    public ResponseEntity<List<User>> findAll() throws NoContentException {
         List<User> list = userService.findAll();
         return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    /**
     * Retrieves information about the currently authenticated user.
     * @param id The ID of the user.
     * @return The user details for the specified ID.
     * @throws NotFoundException If the user is not found.
     */

    @GetMapping("/{id}")
    @Operation(summary = "Get a specific user.")
    @ApiResponse(responseCode = "200", description = "Display user.")
    public ResponseEntity<UserDtoRes> findById(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException {
        UserDtoRes user = userService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }

    /**
     * Modifies a specific user.
     * @param user The user data to be updated.
     * @return The updated user.
     * @throws MissingArgumentException If required arguments are missing.
     */

    @PutMapping
    @Operation(summary = "Modify a specific user.")
    @ApiResponse(responseCode = "200", description = "User has been modified.")
    public ResponseEntity<User> updateUser(
            @Parameter(required = true)
            @RequestBody User user)
            throws MissingArgumentException {
        return ResponseEntity.status(HttpStatus.OK).body(userService.updateUser(user));
    }

    /**
     * Deletes a specific user.
     * @param id The ID of the user to be deleted.
     * @return The deleted user.
     * @throws NotFoundException If the user is not found.
     */

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a specific user.")
    @ApiResponse(responseCode = "200", description = "User has been deleted.")
    public ResponseEntity<User> deleteUser(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException {
        User deletedUser = userService.deleteUser(id);
        return ResponseEntity.status(HttpStatus.OK).body(deletedUser);
    }
}
