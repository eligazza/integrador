package com.guider.controller;

import com.guider.dtos.request.TagDtoReq;
import com.guider.dtos.response.TagDtoRes;
import com.guider.exceptions.*;
import com.guider.service.impl.TagService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller class for managing tour tags.
 * Handles operations related to adding, removing and retrieving tags from tours.
 */

@RestController
@RequestMapping("/tags")
@Tag(name = "Tags")
@SecurityRequirement(name = "bearer-key")
public class TagController {

    // Attributes
    private final TagService tagService;

    /**
     * Constructs an instance of {@code TagController} with the provided {@code TagService}.
     * @param tagService The service responsible for managing tags.
     */

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    /**
     * Retrieves all saved tags (Needs authentication).
     * @return A list of all saved tags.
     */

    @GetMapping()
    @Operation(summary = "Get all saved tags.")
    @ApiResponse(responseCode = "200", description = "Displaying all saved tags.")
    public ResponseEntity<List<TagDtoRes>> findAll() throws NoContentException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.findAll());
    }

    /**
     * Retrieves a specific tag by ID (Needs authentication).
     * @param id The ID of the tag to retrieve.
     * @return The tag with the specified ID.
     */

    @GetMapping("/{id}")
    @Operation(summary = "Find a specific tag.")
    @ApiResponse(responseCode = "200", description = "Display tag.")
    public ResponseEntity<TagDtoRes> findById(
            @Parameter(required = true)
            @PathVariable Long id)
            throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.findById(id));
    }

    /**
     * Saves a new tag (Needs authentication).
     * @param tagDtoReq The tag data to be saved.
     * @return The newly created tag.
     */

    @PostMapping()
    @Operation(summary = "Creates a new tag.")
    @ApiResponse(responseCode = "200", description = "New tag created.")
    public ResponseEntity<TagDtoRes> save(
            @Parameter(required = true)
            @RequestBody TagDtoReq tagDtoReq)
            throws InvalidArgumentException, MissingArgumentException, NotFoundException, DuplicatedException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.save(tagDtoReq));
    }

    /**
     * Updates/modifies an existing tag (Needs authentication).
     * @param tagDtoReq The tag data to be updated (including the id).
     * @return The updated tag.
     */

    @PutMapping()
    @Operation(summary = "Update/Modify an existing tag.")
    @ApiResponse(responseCode = "200", description = "Tag has been updated.")
    public ResponseEntity<TagDtoRes> update(
            @Parameter(required = true)
            @RequestBody TagDtoReq tagDtoReq)
            throws InvalidArgumentException, MissingArgumentException, NotFoundException, DuplicatedException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.update(tagDtoReq));
    }

    /**
     * Deletes an existing tag (Needs authentication).
     * @param id The ID of the tag to delete.
     * @return The deleted tag.
     */

    @DeleteMapping("/{id}")
    @Operation(summary = "Deletes an existing tag.")
    @ApiResponse(responseCode = "200", description = "Tag has been deleted.")
    public ResponseEntity<TagDtoRes> delete(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.delete(id));
    }

    /**
     * Retrieves all tags for a specific tour (Needs authentication).
     * @param tourId The ID of the tour.
     * @return A list of tags for the specified tour.
     */

    @GetMapping(path = "/by-tour-id", params = "tourId")
    @Operation(summary = "Get all tags for a specific tour.")
    @ApiResponse(responseCode = "200", description = "Displaying all tags for this tour.")
    public ResponseEntity<List<TagDtoRes>> findByTourId(
            @Parameter(required = true)
            @RequestParam Long tourId)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException, NoContentException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.findByTourId(tourId));
    }

    /**
     * Adds a tag to a specific tour (Needs authentication).
     * @param tourId The ID of the tour.
     * @param tagId The ID of the tag to be added.
     * @return A list of tags after adding the specified tag to the tour.
     */

    @PostMapping(path = "/add")
    @Operation(summary = "Add a tag to a specific tour.")
    @ApiResponse(responseCode = "200", description = "Tag has been added.")
    public ResponseEntity<List<TagDtoRes>> addTag(
            @Parameter(required = true)
            @RequestParam Long tourId,
            @Parameter(required = true)
            @RequestParam Long tagId)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.addTagToTour(tourId, tagId));
    }

    /**
     * Removes a tag from a specific tour (Needs authentication).
     * @param tourId The ID of the tour.
     * @param tagId The ID of the tag to be removed.
     * @return A list of tags after removing the specified tag from the tour.
     */

    @DeleteMapping(path = "/remove")
    @Operation(summary = "Remove a tag from a specific tour.")
    @ApiResponse(responseCode = "200", description = "Tag has been removed.")
    public ResponseEntity<List<TagDtoRes>> removeTag(
            @Parameter(required = true)
            @RequestParam Long tourId,
            @Parameter(required = true)
            @RequestParam Long tagId)
            throws InvalidArgumentException, MissingArgumentException, NotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(tagService.removeTagFromTour(tourId, tagId));
    }
}
