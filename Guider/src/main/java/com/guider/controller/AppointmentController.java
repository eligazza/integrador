package com.guider.controller;

import com.guider.dtos.request.AppointmentDtoReq;
import com.guider.dtos.response.AppointmentDtoRes;
import com.guider.exceptions.*;
import com.guider.service.impl.AppointmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Controller class for managing appointments.
 * Handles operations related to retrieving, creating, updating, and deleting appointments.
 */

@RestController
@RequestMapping("/appointments")
@Tag(name = "Appointments")

public class AppointmentController {

    // Attributes
    private final AppointmentService appointmentService;

    /**
     * Constructs an instance of {@code AppointmentController} with the provided {@code appointmentService}.
     * @param appointmentService The service responsible for managing appointments.
     */

    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    /**
     * Retrieves all stored appointments.
     * @return A list of all appointments.
     * @throws NoContentException If no appointments are found.
     */

    @GetMapping()
    @Operation(summary = "Get all stored appointments.")
    @ApiResponse(responseCode = "200", description = "Display all stored appointments.")
    public ResponseEntity<List<AppointmentDtoRes>> findAll() throws NoContentException {
        List<AppointmentDtoRes> appointments = appointmentService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(appointments);
    }

    /**
     * Saves a new appointment (makes a reservation).
     * No appointments can be saved if the wanted date is in the past (before today)
     * No appointments can be saved if the tour is already booked by other user (as long as the reservation lasts)
     * No appointments can be saved if user already has another appointment in the specified date and time (as long as the reservation lasts)
     * @param appointmentsDtoReq The appointment data to be saved.
     * @return The newly created appointment.
     * @throws DuplicatedException      If an appointment exists for that date/time.
     * @throws NotFoundException        If referenced entities (user) is not found.
     * @throws InvalidArgumentException If the provided data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     */

    @PostMapping
    @Operation(summary = "Save appointment (make a reservation).")
    @ApiResponse(responseCode = "201", description = "Appointment has been created.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<AppointmentDtoRes> save(
            @Parameter(required = true)
            @RequestBody AppointmentDtoReq appointmentsDtoReq)
            throws DuplicatedException, NotFoundException, InvalidArgumentException, MissingArgumentException, MessagingException {
        AppointmentDtoRes newAppointment = appointmentService.save(appointmentsDtoReq);
        return ResponseEntity.status(HttpStatus.CREATED).body(newAppointment);
    }

    /**
     * Updates/modifies an existing appointment.
     * No appointments can be saved if the wanted date is in the past (before today)
     * No appointments can be saved if the tour is already booked by other user (as long as the reservation lasts)
     * No appointments can be saved if user already has another appointment in the specified date and time (as long as the reservation lasts)
     * @param appointmentsDtoReq The appointment data to be updated (including the id).
     * @return The updated appointment.
     * @throws DuplicatedException      If there is an appointment for the given date/time.
     * @throws NotFoundException        If referenced entities (tour, user, guide) are not found.
     * @throws InvalidArgumentException If the provided data is invalid.
     * @throws MissingArgumentException If required arguments are missing.
     */

    @PutMapping
    @Operation(summary = "Modify an existing appointment.")
    @ApiResponse(responseCode = "200", description = "Appointment has been modified.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<AppointmentDtoRes> update(
            @Parameter(required = true)
            @RequestBody AppointmentDtoReq appointmentsDtoReq)
            throws DuplicatedException, NotFoundException, InvalidArgumentException, MissingArgumentException, MessagingException {
        AppointmentDtoRes updatedAppointment = appointmentService.update(appointmentsDtoReq);
        return ResponseEntity.status(HttpStatus.OK).body(updatedAppointment);
    }

    /**
     * Deletes an appointment.
     * @param id The ID of the appointment.
     * @return The deleted appointment.
     * @throws NotFoundException If the appointment is not found.
     */

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a specific appointment.")
    @ApiResponse(responseCode = "200", description = "Appointment has been deleted.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<AppointmentDtoRes> delete(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException, InvalidArgumentException, MissingArgumentException {
        AppointmentDtoRes deletedAppointment = appointmentService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body(deletedAppointment);
    }

    /**
     * Retrieves a specific appointment by ID.
     * @param id The ID of the appointment to retrieve.
     * @return The appointment with the specified ID.
     * @throws NotFoundException If the appointment is not found.
     */

    @GetMapping("/{id}")
    @Operation(summary = "Find a specific appointment.")
    @ApiResponse(responseCode = "200", description = "Display appointment.")
    public ResponseEntity<AppointmentDtoRes> findById(
            @Parameter(required = true)
            @PathVariable Long id)
            throws NotFoundException {
        AppointmentDtoRes appointment = appointmentService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(appointment);
    }

    /**
     * Retrieves all appointment for a specific guide.
     * @param id The ID of the guide.
     * @return A list of appointments for specified guide.
     * @throws NoContentException If there are no appointments for specified guide.
     */

    @GetMapping("/by-guide-id")
    @Operation(summary = "Get all the appointments for a specific guide.")
    @ApiResponse(responseCode = "200", description = "Display appointments for the specified guide.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<List<AppointmentDtoRes>> findByGuideId(
            @Parameter(name = "guideId", required = true)
            @RequestParam("guideId") Long id)
            throws NoContentException {
        List<AppointmentDtoRes> appointments = appointmentService.findByGuideId(id);
        return ResponseEntity.status(HttpStatus.OK).body(appointments);
    }

    /**
     * Retrieves all appointment for a specific user.
     * @param id The ID of the user.
     * @return A list of appointments for specified user.
     * @throws NoContentException If there are no appointments for specified user.
     */

    @GetMapping("/by-user-id")
    @Operation(summary = "Get all the appointments for a specific user.")
    @ApiResponse(responseCode = "200", description = "Display appointments for the specified user.")
    @SecurityRequirement(name = "bearer-key")
    public ResponseEntity<List<AppointmentDtoRes>> findByUserId(
            @Parameter(required = true)
            @RequestParam("userId") Long id)
            throws NoContentException {
        List<AppointmentDtoRes> appointments = appointmentService.findByUserId(id);
        return ResponseEntity.status(HttpStatus.OK).body(appointments);
    }

    /**
     * Retrieves all appointment for a specific tour.
     * @param id The ID of the tour.
     * @return A list of appointments for specified tour.
     * @throws NoContentException If there are no appointments for specified tour.
     */

    @GetMapping("/by-tour-id")
    @Operation(summary = "Get all the appointments for a specific tour.")
    @ApiResponse(responseCode = "200", description = "Display appointments for the specified tour.")
    public ResponseEntity<List<AppointmentDtoRes>> findByTourId(
            @Parameter(required = true)
            @RequestParam("tourId") Long id)
            throws NoContentException {
        List<AppointmentDtoRes> appointments = appointmentService.findByTourId(id);
        return ResponseEntity.status(HttpStatus.OK).body(appointments);
    }

    /**
     * Retrieves appointments within a specified period.
     * @param start The start date of the period (format: dd-MM-yyyy).
     * @param end   The end date of the period (format: dd-MM-yyyy).
     * @return A list of appointments within the specified period.
     */

    @GetMapping("/by-dates")
    @Operation(summary = "Get all the appointments for a period of time.")
    @ApiResponse(responseCode = "200", description = "Display appointments.")
    public ResponseEntity<List<AppointmentDtoRes>> findByDates(
            @Parameter(description = "Period start date (format: dd-MM-yyyy)", required = true)
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate start,
            @Parameter(description = "Period end date (format: dd-MM-yyyy)", required = true)
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate end) {
        List<AppointmentDtoRes> appointments = appointmentService.findByDates(start, end);
        return ResponseEntity.status(HttpStatus.OK).body(appointments);
    }
}
