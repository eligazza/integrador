package com.guider.utils;

import com.guider.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserLoader implements ApplicationRunner {

    @Autowired
    public UserLoader(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    private UserRepository userRepository;

    public void run(ApplicationArguments args) throws Exception {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode("password");
        BCryptPasswordEncoder passwordEncoder2 = new BCryptPasswordEncoder();
        String hashedPassword2 = passwordEncoder2.encode("password2");
        //userRepository.save(new User("Diegoooo", "diego", "diego22@digital.com", hashedPassword, Role.ADMIN));
        //userRepository.save(new User("Paula", "paula", "paula22@digital.com", hashedPassword2, Role.USER));
    }


}