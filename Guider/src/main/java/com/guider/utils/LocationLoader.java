package com.guider.utils;

import com.fasterxml.jackson.annotation.SimpleObjectIdResolver;
import com.guider.persistence.entity.Location;
import com.guider.persistence.repository.ILocationRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;

@Component
public class LocationLoader {

    private final ILocationRepository locationRepository;

    public LocationLoader(ILocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    // Only uncomment this @PostConstruct line if we want to populate the Location table with information in locations.csv
    // I run it once, now I am going to leave it commented out.
    // @PostConstruct
    public void loadData() {
        try {
            InputStream inputStream = getClass().getResourceAsStream("/locations.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            // Assuming CSV format: name, longitude, latitude
            List<Location> locations = reader.lines()
                    .skip(1) // Skip header line
                    .map(line -> {
                        String[] parts = line.split(";");
                        if (parts.length < 3) {
                            System.out.println("COULD NOT UPLOAD LOCATIONS");
                            return null; // Return null for invalid entries
                        } else {
                            Location location = new Location();
                            location.setName(parts[0]);
                            location.setLongitude(Double.parseDouble(parts[1]));
                            location.setLatitude(Double.parseDouble(parts[2]));
                            return location;
                        }
                    })
                    .filter(Objects::nonNull) // Filter out null entries
                    .toList();

            locationRepository.saveAll(locations);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
